<?php get_header(); ?>

<div class="content wrapper">

    <?php # get_template_part('breadcrumbs'); ?>
    
    <?php
    $parents = ehs_get_page_parents( get_the_ID() );
    $top_page = get_page($parents['top']); ?>

    <h1>404 Error</h1>

    <div class="grid">
        
        <div class="sidebar col-1-4">

            <div class="module">
                <h4>Need more help?</h4>
                <p>If you think you've reached this page in error, or if you can't find the information you need, we'd love to know.</p>
                <p>Please <a href="<?php echo get_the_permalink( get_page_by_title('Contact')); ?>">contact us</a> and let us know what we can do to help.</p>
            </div>
            
        </div>

        <div class="main col-2-3">

            <h2>Sorry, we can't find that page!</h2>

            <p>Oops! It looks like we've lost the page you were looking for. Either someone gave you an outdated link, or you've made a typo in your URL.</p>
            <p>Please check your URL bar for any mistakes, or try searching for the page you need.</p>

            <div class="cu-search">
                <span style="color: white;">What are you looking for?</span>
                <?php get_search_form(); ?>
            </div>

        </div>

    </div>

</div>

<?php get_footer(); ?>