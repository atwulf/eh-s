module.exports = function(grunt){

  grunt.initConfig({
    open : {
      local : {
        path: 'http://localhost:8888/ehs/wp-admin',
        app: 'Google Chrome'
      },
      live : {
        path : 'http://ehs.colorado.edu/wp-admin',
        app: 'Google Chrome'
      },
      issues : {
        path : 'https://bitbucket.org/atwulf/eh-s/issues?status=new&status=open&sort=-priority',
        app: 'Google Chrome'
      },
      git : {
        path : '/Applications/MAMP/htdocs/ehs/wp-content/themes/ehs',
        app: 'Terminal'
      },
      sourcetree : {
        path : '/Applications/MAMP/htdocs/ehs/wp-content/themes/ehs',
        app: 'SourceTree'
      }
    }
  });

  grunt.loadNpmTasks('grunt-open');

};