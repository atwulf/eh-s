<?php get_header(); ?>

<div class="content wrapper">

    <?php get_template_part('breadcrumbs'); ?>

    <h1>News and Updates</h1>

    <div class="grid">

        <?php get_sidebar(); ?>

        <div class="main col-2-3">

            <h2>Latest Posts in <?php single_term_title(); ?></h2>

            <?php if ( have_posts() ) {
                echo '<ul class="home-loop">';

                while ( have_posts() ) {

                    the_post();
                    get_template_part('entry');

                }

                echo '</ul>';
            } else {

                echo '<p>Sorry, nothing was found for your search.</p>';

            } ?>

        </div>

    </div>

</div>

<?php get_footer(); ?>