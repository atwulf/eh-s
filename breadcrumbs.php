<?php // Query strings
if ( isset($_GET['page']) ) {
    $query = $_GET['page'];
    $titles = array(
        'quiz' => 'Completion Quiz',
        'list' => 'Comprehensive List',
        'course_material' => 'Course Material'
    );

    if ( array_key_exists($query, $titles) ) {
        $page = $titles[$query];
    } else {
        $page = '';
    }
}
?>

<div class="breadcrumbs">
    
    <a href="<?php echo esc_url( home_url('/') ); ?>">Home</a>

    <?php
    $parents = array_reverse( get_post_ancestors( get_the_ID() ) );

    if ( sizeof( $parents ) > 0 && ! isset($_GET['s']) ) {
    foreach ( $parents as $parent ) { // Get all parent pages

        echo '&nbsp;&gt; <a href="'. get_the_permalink($parent) .'">' . get_the_title($parent) .'</a>';

    } ?>

        <?php } elseif ( get_post_type() == 'trainings' && ! isset($_GET['s']) ) {
            // Breadcrumbs for Trainings CPT Single Post ?>

            &nbsp;&gt; <a href="<?php echo get_permalink( get_page_by_title('Training') ); ?>">Training</a>

        <?php } elseif ( get_post_type() == 'resources' && ! isset($_GET['s']) ) {
            // Breadcrumbs for Resources CPT Single Post ?>
            
            &nbsp;&gt; <a href="<?php echo get_permalink( get_page_by_title('Resources') ); ?>">Resources</a>
        <?php } elseif ( get_post_type() == 'post' && ! isset($_GET['s']) || is_archive() ) {
            // Blog posts are under News and Updates ?>
            
            &nbsp;&gt; <a href="<?php echo home_url('/news-and-updates'); ?>">News and Updates</a>
        <?php } ?>

    <?php if ( is_single() || is_page() ) { ?>
        &nbsp;&gt; <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    <?php } ?>

    <?php if ( isset($_GET['page']) && $page !== '' ) {
        // Get query string page name if it exists ?>

        &nbsp;&gt; <a href="<?php the_permalink(); echo '?page=' . $_GET['page']; ?>"><?php echo $page; ?></a>

        <?php } elseif ( isset($_GET['s']) ) {
            // Breadcrumb for search ?>

            &nbsp;&gt; <a href=""><?php echo 'Search results for: '. get_search_query(); ?></a>
        <?php } ?>

</div>

<?php if ( current_user_can('edit_post', get_the_ID()) ) {
    echo '<a href="'. get_edit_post_link( get_the_ID(), '' ) . '" class="button-secondary right">Edit This Page</a>';
    } ?>