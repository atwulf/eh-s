$(document).ready(function(){

/* ** MINIFY ALL SCRIPTS BEFORE PRODUCTION **
 |-------------------
 | CONTENTS
 |-------------------
 | =Modernizr, =Responsive, =Filters,
 | =Table of Contents, =Answered Questions,
 | =Radioactive Containers
 | =window.resize
 |-------------------
 */

/*
 |--------------
 | =Modernizr
 |--------------
 */

/**
 * Available Modernizr Tests
 *
 * - SVG
 * - html5shiv v3.7
 */

/*
 |--------------
 | =Responsive
 |--------------
 */

/**
 * These bits control the responsive design
 * stuff that can't be handled by CSS alone.
 */

function responsive() {

// Home
if ( $('body').hasClass('home') ) {
    var $subCta = $('.sub-cta-home'),
        $subCtaCol = $subCta.find('.col-3-4'),
        $emergencyCta = $('.emergency-home');

    // When the Emergency Contact module is hidden,
    // remove the other modules from the inner grid
    if ( ! $emergencyCta.is(':visible') ) {
        if ( $subCta.hasClass('grid') ) {
            $subGrid = $subCtaCol.find('.grid').detach();
            $subCta.removeClass('grid').prepend($subGrid);
        }
    } else {
        if ( ! $subCta.hasClass('grid') ) {
            $subGrid = $subCta.find('.grid').detach();
            $subCta.addClass('grid');
            $subCtaCol.append($subGrid);
        }
    }
}

// Sidebar
if ( $('.sidebar').length > 0 ) {
    var $sidebar = $('.sidebar'),
        $grid = $sidebar.parent('.grid');
        $subpages = $sidebar.find('.subpages');

    // When columns are 100% width, put the second
    // sidebar module at the bottom of content
    if ( $sidebar.css('float') == 'none' ) {
        if ( $sidebar.find('.more-info').length > 0 ) {
            $moreInfo = $sidebar.find('.more-info').detach();
            $grid.append($moreInfo);
            $moreInfo.wrap('<div class="col-1-4"></div>');
        }
    } else {
        if ( $sidebar.find('.more-info').length === 0 ) {
            $moreInfo = $('.more-info').parent('.col-1-4').detach();
            $moreInfo = $moreInfo.find('.more-info').unwrap();
            $subpages.after($moreInfo);
        }
    }
}

// Navigation
var $nav = $('.menu'),
    $navItems = $nav.find('.menu-item'),
    $cuSearch = $('.cu-search');

// When menu items are display: block, put
// the menu and search in a drawer
if ( $navItems.css('display') == 'none' ) {
    if ( $nav.find('select').length === 0 ) {
        $nav.append('<div class="right"><span>Menu: </span><select class="touch-nav"><option value="null">-- Navigation --</option></select>');
        $.each($navItems, function() {
            var $link = $(this).find('a'),
                $selected = '';
            if ( $(this).hasClass('current') ) $selected = ' selected';

            $('.touch-nav').append('<option value="'+ $link.attr('href') +'"'+ $selected +'>'+ $link.text() +'</option>');
        });

        $('.touch-nav').change(function(){
            $(location).attr('href', $('.touch-nav').val());
        });
    }
} else {
    if ( $nav.find('select').length > 0 ) {
        $('.menu .right').remove();
    }
}

// Hidden TOC
if ( $('.toc-list').length > 0 ) {
    $('.hide-toc').text('(hide)');
    $('.toc-list').slideDown();
}

} /* END responsive() */

// Call the function
responsive(); /* document.ready */
$(window).resize(function(){ responsive(); });

/*
 |--------------
 | =Filters
 |--------------
 */

/**
 * This function uses checkboxes to filter
 * items in a list.
 */

// First, grab any checkbox that says it's a filter
var $trFilters = $('.filters');

if ( $trFilters ) {
    // Get filterable items
    var $items = $('.page-loop-post');

    // If filters exist, watch them for state change
    $trFilters.change(function(){
        var query = '?',
            filter = $(this).attr('name'),
            term = $(this).val();

        // get existing query string
        // ex. getStr['a'] === $_GET['a']
        var getStr = [], hash;
        var q = document.URL.split('?')[1];
        if ( q !== undefined ) {
            q = q.split('&');
            for (var i=0; i<q.length; i++) {
                hash = q[i].split('=');
                getStr[hash[0]] = hash[1];
            }
        }
        // remove page number from query strng
        if ( getStr.pgn !== undefined ){
            currPage = getStr.pgn;
            getStr.splice('pgn', 1);
        }

        // create string from query options
        if ( filter == 'intended_for' ) getStr.f = term;
        if ( filter == 'research_with' ) getStr.w = term;

        if ( getStr.v !== undefined ) {
            query = query.concat('v='+getStr.v+'&');
        }

        if ( getStr.f !== undefined ) {
            query = query.concat('f='+getStr.f+'&');
        }

        if ( getStr.w !== undefined ) {
            query = query.concat('w='+getStr.w+'&');
        }

        if ( query.slice(-1) == '&' )
            query = query.substring(0, query.length - 1);

        var newLoc = document.URL.split('?')[0];
        window.location = newLoc + query;

    });
}

/*
 |--------------
 | =Resources Pagination
 |--------------
 */

/**
 * Relevanssi and custom post type loops don't play well together,
 * so this is a jQuery function that paginates any set of elements.
 */

var $resourcesList = $('.resources-list'),
    perPage = 10,
    counter = 0,
    loopCount = 0,
    pages = [],
    page = [];

if ( $resourcesList.length > 0 ) {
    // Paginate if there are more items than we want
    if ( $('.resource-single').length > perPage ) {
        // grab the items and chunk them into page arrays
        var items = $('.resource-single').detach();

        items.each(function(){
            page.push( $(this) );
            counter = counter + 1;
            loopCount = loopCount + 1;

            if ( counter == perPage ) { // full page
                pages.push(page);
                page = []; // reset page and count
                counter = 0;
            }

            if ( loopCount == items.length ) { // grab leftover items
                pages.push(page);
                page = [];
                counter = 0;
                loopCount = 0;
            }
        });

        // create pagination links
        var currPage = 1,
            totalPage = pages.length,
            query = '?';

        // get existing query string
        // ex. getStr['a'] === $_GET['a']
        var getStr = [], hash;
        var q = document.URL.split('?')[1];
        if ( q !== undefined ) {
            q = q.split('&');
            for (var i=0; i<q.length; i++) {
                hash = q[i].split('=');
                getStr[hash[0]] = hash[1];
            }
        }
        // remove page number from query strng
        if ( getStr.pgn !== undefined ){
            currPage = getStr.pgn;
            getStr.splice('pgn', 1);
        }
        // create string from query options
        if ( getStr.kw !== undefined )
            query = query.concat('kw='+getStr.kw+'&');
        if ( getStr.date !== undefined )
            query = query.concat('date='+getStr.date+'&');
        if ( getStr.con !== undefined )
            query = query.concat('con='+getStr.con+'&');
        if ( getStr.type !== undefined )
            query = query.concat('type='+getStr.type+'&');
        if ( getStr.unit !== undefined )
            query = query.concat('unit='+getStr.unit+'&');
        if ( getStr.sortby !== undefined ) { /* !! This is outputting a function def for .sort(), please fix !! */
            $sort = getStr.sortby;
            query = query.concat('sortby='+$sort+'&');
        }
        if ( getStr.order !== undefined )
            query = query.concat('order='+getStr.order+'&');

        // Append page of items
        $resourcesList.append(pages[currPage - 1]);

        // append page numbers
        $resourcesList.after('<div class="pagination"></div');

        // Previous link
        if ( currPage > 1 ) {
            var prevPage = parseInt(currPage) - 1;
            $('.pagination').append('<a class="page-numbers" href="'+query+'pgn='+prevPage+'">« Previous</a>');
        }

        // Pages
        for (var x = 1; x <= totalPage; x++) {
            if ( x == currPage ) {
                $('.pagination').append('<span class="page-numbers current">'+x+'</a>');
            } else {
                $('.pagination').append('<a class="page-numbers" href="'+query+'pgn='+x+'">'+x+'</a>');
            }
        }

        // Next link
        if ( currPage < totalPage ) {
            var nextPage = parseInt(currPage) + 1;
            $('.pagination').append('<a class="page-numbers" href="'+query+'pgn='+nextPage+'">Next »</a>');
        }
    }
}

/*
 |--------------
 | =Table of Contents
 |--------------
 */

/**
 * This function dynamically creates a table of contents from
 * an object? array? of heading tags
 */

var $toc = $('.toc-list'),
    $article = $('.resource-main');

if ( $toc.length > 0 && $article.length > 0 ) {
    var $headers = $article.children("h1, h2, h3");

    $headers.each(function() {
        var text = $(this).text(),
            id = text.replace(/&.*?;/g, '').replace(/[^\w]/g, '').toLowerCase();

        $(this).attr('id', id);

        if ( $(this).is('h1') ) {
            $toc.append('<li><a href="#'+id+'">'+text+'</a></li>');

        } else if ( $(this).is('h2') ) {
            if ( $toc.find('> ol:last-child').length === 0 ) $toc.append('<ol></ol>');
            $toc.find('> ol:last-child')
                .append('<li><a href="#'+id+'">'+text+'</a></li>');

        } else {
            if ( $toc.find('> ol:last-child').find('ol').length === 0 )
                $toc.find('> ol:last-child')
                    .append('<ol></ol>');
            $toc.find('> ol:last-child').find('> ol:last-child')
                .append('<li><a href="#'+id+'">'+text+'</a></li>');
        }
    });

    var hide = $('.toc .hide-toc');
    if ( hide.length > 0 ) {
        hide.on('click', function(){
            $('.toc-list').slideToggle();

            if ( $(this).text() == '(hide)' ) {
                $(this).text('(show)');
            } else {
                $(this).text('(hide)');
            }
        });
    }
}

/*
 |--------------
 | =Answered Questions
 |--------------
 */

/**
 * Count how many questions are on a page.
 * Divide that number by the number of "answered" questions
 * Size a progress bar appropriately
 */

var progress = {
        bar: $('.progress .bar'),
        counter: $('.progress .count')
    },
    questions = $('.question-single').length,
    answers = $('.question-single input'),
    textarea = $('.question-single textarea'),
    textareaFill = 0,
    answered = 0;

if ( progress.bar.length > 0 ) {
    progress.bar.css('width', '0');
    progress.counter.text('0 of '+questions);
    answers.on('change', function(){
        // Count textareas that have input value
        var textareaFill = 0;
        textarea.each(function(){
            if ( $(this).val() ) textareaFill = textareaFill + 1;
        });

        // Count all answered questions
        answered = $('input:radio:checked').length + textareaFill;

        // Size progress bar
        var width = Math.round((answered / questions) * 100);
        progress.bar.css('width', width+'%');
        progress.counter.text(answered+' of '+questions);
    });
    textarea.on('blur', function(){
        // Count textareas that have input value
        var textareaFill = 0;
        textarea.each(function(){
            if ( $(this).val() ) textareaFill = textareaFill + 1;
        });

        // Count all answered questions
        answered = $('input:radio:checked').length + textareaFill;

        // Size progress bar
        var width = Math.round((answered / questions) * 100);
        progress.bar.css('width', width+'%');
        progress.counter.text(answered+' of '+questions);
    });
}

/*
 |--------------
 | =Radioactive Containers
 |--------------
 */

/**
 * Make sure no more than 5 containers can be submitted.
 * Repeat container type inputs on request.
 */

var displayTotal = $('#container-count'), // counter output div
    containerTypes = $('.container-request'); // individual container fieldset

if ( displayTotal.length > 0 ) {
    // Count types of container
    var typesCount = containerTypes.length,
        containersCount = typesCount; // init

    // Update counter on form
    displayTotal.text(containersCount);

    // Duplicate fieldset on request
    $('.add-container').on('click', function(){
        var newContainer = $('.container-request:last-of-type').clone(true);
        if ( containersCount < 5) typesCount = (parseInt(typesCount) + 1).toString();
        newContainer.css('display', 'none');

        // Update all names, ids, and label fors
        var fields = newContainer.find('.container-fieldset');
        newContainer.find('h4')
            .text('Container '+ typesCount);
        if ( newContainer.find('.remove-container').length < 1 ) {
            newContainer.find('h4').after('<span class="remove-container">Remove Container</span>');
        }

        // Loop through container-fieldset items
        $.each( fields, function(){
            var field = $(this),
                string = field.attr('name'),
                label = field.parents('label');

            // Update string
            string = string.split('-');
            string[ string.length - 1 ] = typesCount;
            string = string.join('-');

            // Update attrs
            field.attr('name', string);
            field.attr('id', string);
            label.attr('for', string);

            // reset val
            if ( field.is('input:not([type="radio"])') ) field.val('');
            if ( field.is('textarea') ) field.text('');
        });

        // Add new container fieldset
        if (containersCount < 5) {
            newContainer.insertAfter($('.container-request:last-of-type'));
            newContainer.slideDown();

            // Update containers count
            containersCount = containersCount + 1;
            displayTotal.text(containersCount.toString());
            $('input[name="container-types-count"]').val(typesCount);
        } else {
            $('.add-container').addClass('disabled');
        }
    });

    // Prevent form submission if there are more than 5 containers
    $('.quiz').on('submit', function(e){
        if ( containersCount > 5 ) {
            e.preventDefault();
            alert('Please make sure you have no more than 5 containers in your request.');
        }
    });

    // Remove container types on request
    $(document).on('click', '.remove-container', function(){
        var container = $(this).parents('.container-request'),
            numRemoved = 1;

        // Update containers count
        typesCount = typesCount - 1;
        containersCount = containersCount - numRemoved;
        displayTotal.text(containersCount.toString());

        // // Update counters
        // containerCounters = $('.container-type-counter');
        // containerCounters.on('change', function(){
        //     containersCount = 0; // reset to recount
        //     containerCounters.each(function(){
        //         containersCount = parseInt(containersCount) + parseInt($(this).val());
        //     });

        //     displayTotal.text(containersCount.toString());
        // });

        // Remove disabled class from .add-container when applicable
        if ( containersCount < 5 ) $('.add-container').removeClass('disabled');

        // Remove container
        container.slideUp(function(){
            $(this).remove();
        });
    });
}



}); // end












