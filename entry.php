<?php if ( isset($_GET['s']) ) {

    get_template_part('templates/entry/search');

} else if ( get_post_type() == 'trainings' ) {

    get_template_part('templates/entry/training');

} else if ( get_post_type() == 'training_questions' ) {

    get_template_part('templates/entry/question');

} else if ( get_post_type() == 'resources' ) {

    get_template_part('templates/entry/resource');

} else if ( get_post_type() == 'audits' ) {

    get_template_part('templates/entry/audit');

} else if ( get_post_type() == 'staff' ) {

    get_template_part('templates/entry/staff');

} else if ( get_post_type() == 'page' ) {

    get_template_part('templates/entry/page');

} else {

    get_template_part('templates/entry/post');

} ?>