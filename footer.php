    <div class="footer" role="contentinfo">
        <div class="wrapper">

            <div class="left">

                <h6>Environmental Health &amp; Safety</h6>
                <p>1000 Regent Drive<br />
                413 UCB<br />
                University of Colorado<br />
                Boulder, CO 80309</p>
                <p>Phone: <a href="tel:13034926025">(303) 492-6025</a><br />
                Fax: (303) 492-2854<br />
                Email: <a href="mailto:ehs@colorado.edu">ehs@colorado.edu</a></p>

            </div>

            <div class="right">

                <div class="be-boulder">
                    <img src="<?php bloginfo('template_url'); ?>/img/be-boulder.svg" alt="Be Informed. Be prepared. Be Fearless. Be Boulder.">
                </div>

                <p>&copy; Regents of the University of Colorado<br />
                <a href="http://www.colorado.edu/about/legal-trademarks" target="_blank">Legal &amp; Trademarks</a> &middot; <a href="http://www.colorado.edu/about/privacy-statement" target="_blank">Privacy</a></p>

            </div>
            <div class="cf"></div>

        </div>
    </div>

    <?php wp_footer(); ?>

</body>
</html>