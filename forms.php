<?php if ( isset($_POST) ) {

    $query = $_POST['q'];

    // Set common fields
    $common = array(
        // Hidden fields
        'return' => trim($_POST['return']),
        'form-recipient' => trim($_POST['to']),

        // Basic Contact
        'first-name' => trim($_POST['first-name']),
        'last-name' => trim($_POST['last-name']),
        'email' => strip_tags(trim($_POST['email-address'])),
        'phone' => trim($_POST['phone-number']),

        // Lab Support and Trainings
        'supervisor' => trim($_POST['supervisor']),
        'proctor-name' => trim($_POST['proctor-name']),
        'proctor-email' => strip_tags(trim($_POST['proctor-email'])),
        'building' => trim($_POST['building-code']),
        'room' => trim($_POST['room-number']),
        'role' => trim($_POST['role']),
        'title' => trim($_POST['quiz-title'])
    );

    // Set the email headers
    $headers = "From: ". $common['first'] .' '. $common['last'] .' <'. $common['email'] .'>' . "\r\n";
    $headers .= "Reply-To: ". $common['email'] . "\r\n";
    if (!empty($common['proctor-email'])) $headers .= "CC: ". $common['proctor-email'] ."\r\n";
    $headers .= "BCC: ". $common['email'] .", austin@thatbrightrobot.com\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

    // Who is receiving the message?
    session_start();
    if ( !empty($_SESSION['form-recipient']) ) {
        $to = $_SESSION['form-recipient'];
    } else {
        $to = 'ehs@colorado.edu';
    }

    // Setup HTML email template
    $message = '<html><body><style>a { color: #cfb87c; }</style>';
    $table_style = '<table width="500" style="'.
                'margin: 0 auto 20px;'.
                'border: 2px solid #565a5c;'.
                'font-family: sans-serif;'.
                '">';
    $message .= $table_style;

    // Get fields for specific forms
    if ( 'quiz' == $query ) { # Email form for training quizzes

        // Get quiz questions
        $qna = $_POST;
        $common['return'] .= '?page=form';

        // Set the subject to reflect the name of the quiz and who took it
        $subject = '[QUIZ] '. $common['title'] .' from '. $common['first-name'] .' '. $common['last-name'];

        // Set the message
        // NOTE: This overrides the HTML email output that the other forms have.
        // This form's output is unique as the Rad unit requested plain text emails
        $message = '<html><body style="font-family: sans-serif; line-height: 1.4"><table>'; # overwrite table styles

        // Get form data from top down
        $message .= "<p><strong>Quiz Answers for ". $common['title'] ."</strong></p>";

        $message .= "<p>Taken by: ". $common['first-name'].' '.$common['last-name']. "<br/>";
        $message .= "Email: ". $common['email']. "<br/>";
        $message .= "Role: ". $common['role']. "</p>";

        $message .= "<p>Supervisor/PI: ". $common['supervisor']. "<br/>";
        if ( !empty($common['proctor-name']) ) {
            $message .= "Proctor: ". $common['proctor-name'] ."<br/>";
        }
        if ( !empty($common['proctor-email']) ) {
            $message .= "Proctor Email: ". $common['proctor-email'];
        }
        $message .= "Building: ". $common['building']. "<br/>";
        $message .= "Room: ". $common['room']. "</p>";

        $message .= "<div style='width: 500px;'>";

        // Loop through questions and answers
        $count = 1;
        foreach ( $qna as $key => $val ) {
            $key = trim($key);
            $val = trim($val);

            if ( strpos( $key, 'question' ) !== false ) {
                $id = explode('-', $key);
                $id = $id[1];
                $message .= '<p><strong>Question '. $count .': </strong>'. $val .'<br/>';
                $message .= '<em>Answer given: '. html_entity_decode(addslashes($qna['answer-'. $id])) . '</em></p>';
                $count = $count + 1;
            }
        }

        $message .= "</div>";

    }
    elseif ( 'training_signup' == $query ||
             'training_info' == $query ) { # Email form for training signups

        $common['return'] .= '?page=form';

        // Get requested date
        $course_date = trim($_POST['course-date']);

        // Signup vs Learn More
        $signup = 'Sign Up Information';
        if ( $query == 'training_info' ) $signup = 'Information Request';

        // Set the subject to reflect the name of the signup and who sent it
        $subject = $signup .' for '. $common['title'] .' from '. $common['first-name'] .' '. $common['last-name'];

        // Set the message
        $message .= '<tr><th colspan="2" style="padding: 1em; text-align: center; background: #cfb87c; color: #000000; border-bottom: 2px solid #565a5c;">';
        $message .= $signup . ' for<br />';
        $message .= $common['title'] .' Training</th>';

        $message .= '</tr><tr style="background: #e6e6e6;"><td style="padding: .5em 1em;">';
        $message .= 'Name</td><td style="padding: .5em 1em;">';
        $message .= $common['first-name'] .' '. $common['last-name'] .'</td></tr>';

        $message .= '</tr><tr style="background: #565a5c; color: #FFFFFF;"><td style="padding: .5em 1em;">';
        $message .= 'Email Address</td><td style="padding: .5em 1em;">';
        $message .= $common['email'] .'</td></tr>';

        $message .= '<tr style="background: #e6e6e6;"><td style="padding: .5em 1em;">Role</td>';
        $message .= '<td style="padding: .5em 1em;">';
        $message .= $common['role'] .'</td></tr>';

        $message .= '<tr style="background: #565a5c; color: #FFFFFF;"><td style="padding: .5em 1em;">Supervisor</td><td style="padding: .5em 1em;">'. $common['supervisor'] .'</td></tr>';

        if ( $course_date !== '' ) {
            $message .= '<tr style="background: #565a5c; color: #ffffff;"><td style="padding: .5em 1em;">Requested Session</td><td style="padding: .5em 1em;">'. $course_date .'</td></tr>';
        }

    }
    elseif ( 'rad_waste' == $query ) { # Email form for Radiactive Waste Pickup

        // Get additional POST fields
        $rad = $_POST;

        // Temporary fix for Form Recipient issues on this form
        // (Please actually fix this at some point)
        $to = 'radsafety@colorado.edu';

        // Set subject
        $subject = 'Radioactive Waste Pickup Request from '. $common['first-name'].' '.$common['last-name'];

        // Set the message
        // NOTE: This overrides the HTML email output that the other forms have.
        // This form's output is unique as the Rad unit requested plain text emails
        $message = '<html><body style="font-family: sans-serif; line-height: 1.4"><table>'; # overwrite table styles

        // Get form data from top down
        $message .= "<p><strong>Radioactive Waste Pickup Request</strong><br/>";
        $message .= "Date of request: ". date('F j, Y') ."</p>";

        $message .= "<p>Name: ". $common['first-name'].' '.$common['last-name']. "<br/>";
        $message .= "Email: ". $common['email']. "<br/>";
        $message .= "Phone: ". $common['phone']. "</p>";

        $message .= "<p>Supervisor/PI: ". $common['supervisor']. "<br/>";
        $message .= "Department: ". $rad['department']. "<br/>";
        $message .= "Building: ". $common['building']. "<br/>";
        $message .= "Room: ". $common['room']. "</p>";

        $message .= "<p>Wipes have ";
        $message .= ( $rad['wipes'] == 'no' ? 'NOT ' : ' ' );
        $message .= 'been taken.' . "</p>";

        $message .= "<p>Additional Information:<br/>";
        $message .= $rad['message'] ."</p>";

        // Loop through containers
        $types_count = (int)$rad['container-types-count'];
        for ($id = 1; $id <= $types_count; $id++) {

            $containers[] = array(
                'id' => $id,
                'type' => trim($rad['waste-type-'.$id]),
                'volume' => trim($rad['waste-volume-'.$id]),
                'count' => trim($rad['container-count-'.$id]),
                'isotopes' => array(
                    trim($rad['isotope_1-'.$id]) => trim($rad['activity_1-'.$id]),
                    trim($rad['isotope_2-'.$id]) => trim($rad['activity_2-'.$id]),
                    trim($rad['isotope_3-'.$id]) => trim($rad['activity_3-'.$id])
                ),
                'ph' => trim($rad['ph-'.$id]),
                'replacement' => trim($rad['need-replacement-'.$id]),
                'color' => trim($rad['waste-color-'.$id]),
                'contents' => nl2br($rad['constituents-'.$id])
            );

        }

        foreach ( $containers as $container ) {
            if ( !empty($container['type']) ) {

                // Set container color hex code
                if ( $container['color'] == 'Yellow' ) {
                    $color_code = '#FFCC00';
                } elseif ( $container['color'] == 'Green' ) {
                    $color_code = '#00CC00';
                } elseif ( $container['color'] == 'Orange' ) {
                    $color_code = '#FF6600';
                } else {
                    $color_code = '#000000';
                }

                // Build container tables
                $message .= '<tr><th colspan="2" style="padding: 1em; text-align: center;">';
                $message .= 'Container '. $container['id'] .' Details</th>';

                $message .= '</tr><tr><td style="padding: .5em 1em;">';
                $message .= 'Type of Waste</td><td style="padding: .5em 1em;">';
                $message .= $container['type'] .'</td></tr>';

                $message .= '</tr><tr><td style="padding: .5em 1em;">';
                $message .= 'Volume of Waste</td><td style="padding: .5em 1em;">';
                $message .= $container['volume'] .'</td></tr>';

                $message .= '</tr><tr><td style="padding: .5em 1em;">';
                $message .= 'Isotopes</td><td style="padding: .5em 1em;">';

                foreach ( $container['isotopes'] as $key => $val ) {
                    if ( !empty($key) || !empty($val) ) $message .= $key .' at '. $val .'mCi<br/>';
                }

                $message .= '</td></tr>';

                $message .= '</tr><tr><td style="padding: .5em 1em;">';
                $message .= 'Constituents</td><td style="padding: .5em 1em;">';
                $message .= $container['contents'] .'</td></tr>';

                $message .= '</tr><tr><td style="padding: .5em 1em;">';
                $message .= 'pH (for liquid waste)</td><td style="padding: .5em 1em;">';
                $message .= $container['ph'];
                $message .= '</td></tr>';

                $message .= '</tr><tr><td style="padding: .5em 1em;">Container color </td><td style="padding: .5em 1em;">';
                $message .= $container['color'] .'</td></tr>';
                $message .= '<tr><td style="padding: .5em 1em;">';
                $message .= 'Needs replacement container?</td><td  style="padding: .5em 1em;">';
                $message .= $container['replacement'] .'</td></tr>';

            }
        }

    }
    elseif ( 'contact' == $query ) { # Email form for Contact page
        // Get additional POST fields
        $contact = $_POST;

        // Set subject
        $subject = 'Message from '. $common['first-name'].' '.$common['last-name'];

        // Set the message
        $message .= '<tr><th colspan="2" style="padding: 1em; text-align: center; background: #cfb87c; color: #000000; border-bottom: 2px solid #565a5c;">';
        $message .= 'Message from '. $common['first-name'].' '.$common['last-name'].'</th>';

        $message .= '</tr><tr style="background: #e6e6e6;"><td style="padding: .5em 1em;">';
        $message .= 'Name</td><td style="padding: .5em 1em;">';
        $message .= $common['first-name'] .' '. $common['last-name'] .'</td></tr>';

        $message .= '</tr><tr style="background: #565a5c; color: #FFFFFF;"><td style="padding: .5em 1em;">';
        $message .= 'Email Address</td><td style="padding: .5em 1em;">';
        $message .= $common['email'] .'</td></tr>';

        $message .= '<tr style="background: #e6e6e6;"><td style="padding: .5em 1em;">Phone</td>';
        $message .= '<td style="padding: .5em 1em;">';
        $message .= $common['phone'] .'</td></tr>';

        $message .= '<tr style="background: #565a5c; color: #FFFFFF;"><td colspan="2" style="padding: .5em 1em;">Message</td></tr>';

        $message .= '<tr style="background: #e6e6e6;"><td colspan="2" style="padding: .5em 1em;">'. $contact['message'] .'</td></tr>';
    }
    elseif ( 'safety' == $query ) { # Email form for Report a Safety Concern
        // Get additional POST fields
        $safety = $_POST;

        // Set subject
        $subject = 'Safety Concern Report from '. $common['first-name'].' '.$common['last-name'];

        // Set the message
        $message .= '<tr><th colspan="2" style="padding: 1em; text-align: center; background: #cfb87c; color: #000000; border-bottom: 2px solid #565a5c;">';
        $message .= 'Safety Concern Report from '. $common['first-name'].' '.$common['last-name'].'</th>';

        $message .= '</tr><tr style="background: #e6e6e6;"><td style="padding: .5em 1em;">';
        $message .= 'Name</td><td style="padding: .5em 1em;">';
        $message .= $common['first-name'] .' '. $common['last-name'] .'</td></tr>';

        $message .= '</tr><tr style="background: #565a5c; color: #FFFFFF;"><td style="padding: .5em 1em;">';
        $message .= 'Email Address</td><td style="padding: .5em 1em;">';
        $message .= $common['email'] .'</td></tr>';

        $message .= '<tr style="background: #e6e6e6;"><td style="padding: .5em 1em;">Phone</td>';
        $message .= '<td style="padding: .5em 1em;">';
        $message .= $common['phone'] .'</td></tr>';

        $message .= '</tr><tr style="background: #565a5c; color: #FFFFFF;"><td style="padding: .5em 1em;">';
        $message .= 'Supervisor/PI</td><td style="padding: .5em 1em;">';
        $message .= $common['supervisor'] .'</td></tr>';

        $message .= '<tr style="background: #e6e6e6;"><td style="padding: .5em 1em;">Location</td>';
        $message .= '<td style="padding: .5em 1em;">Building: '. $common['building'];
        $message .= '<br/>Room: '. $common['room'] .'</td></tr>';

        $message .= '</tr><tr style="background: #565a5c; color: #FFFFFF;"><td style="padding: .5em 1em;">';
        $message .= 'Role</td><td style="padding: .5em 1em;">';
        $message .= $common['role'] .'</td></tr>';

        $message .= '<tr style="background: #e6e6e6;"><td style="padding: .5em 1em;">Nature of Concern</td>';
        $message .= '<td style="padding: .5em 1em;">';
        $message .= $safety['nature-of-concern'] .'</td></tr>';

        $message .= '<tr style="background: #565a5c; color: #FFFFFF;"><td colspan="2" style="padding: .5em 1em;">Details:</td></tr>';

        $message .= '<tr style="background: #e6e6e6;"><td colspan="2" style="padding: .5em 1em;">'. $safety['concern-details'] .'</td></tr>';
    }

    // Close email template tags and prep for send
    $message .= '</table></body></html>';
    $message = wordwrap($message, 78, "\n");

    // Form output testing
    // echo $message;
    // echo $subject;
    // echo $to;

    // Send message
    if ( mail($to, $subject, $message, $headers) ) {

        $query_append = '?';
        if ( strpos($common['return'], '?') ) $query_append = '&';

        // Set success var
        $common['return'] .= $query_append . 'complete#page-title';
        header("Location: ". $common['return']);
        exit();

    } else {

        // Set error var
        $common['return'] .= $query_append . 'fail#page-title';
        header("Location: ". $common['return']);
        exit();

    }

} ?>