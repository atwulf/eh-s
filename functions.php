<?php
/**
 * THEME INITIALIZATION
 */
add_action('after_setup_theme', 'blankslate_setup');
function blankslate_setup() {
    add_theme_support('automatic-feed-links');
    add_theme_support('post-thumbnails');

    global $content_width;
    if ( ! isset( $content_width ) ) $content_width = 640;
    register_nav_menus(
        array( 'main-menu' => __( 'Main Menu', 'blankslate' ) )
    );
}

// Set default the_title attribute for posts where the title was left blank.
add_filter('the_title', 'blankslate_title');
function blankslate_title($title) {
    if ($title == '') {
        return '&rarr;';
    } else {
        return $title;
    }
}

/**
 * STYLES AND SCRIPTS
 */

// Enqueue jQuery
if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
   wp_deregister_script('jquery');
   wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js", false, null, true);
   wp_enqueue_script('jquery');
}

// Enqueue Theme scripts and styles
if (!is_admin()) add_action("wp_enqueue_scripts", "br_enqueue_scripts");
function br_enqueue_scripts() {
    /* Scripts --- */

    wp_register_script('global-js', get_template_directory_uri() . '/js/global.min.js', array('jquery'), null, true);
    wp_enqueue_script('global-js');

    /* Styles --- */

    wp_register_style('global-css', get_template_directory_uri() . '/css/global.min.css', false, null);
    wp_enqueue_style('global-css');
}

/**
 * WORDPRESS ADMIN CUSTOMIZATIONS
 */

// Remove that unnecessary admin bar from the front end
add_filter('show_admin_bar', '__return_false');

// Allow SVG and other file types to be uploaded as media
add_filter( 'upload_mimes', 'cc_mime_types' );
function cc_mime_types( $mimes ){
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}

// Remove menus from admin dashboard for non Administrator accounts
function remove_menus () {
    global $menu;

    // add top level menus here
    $restricted = array( __('Comments'), __('Tools'), __('SEO'));

    end ($menu);
    while (prev($menu)){
      $value = explode(' ',$menu[key($menu)][0]);
      if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
    }
}
if ( !current_user_can('create_users') ) { add_action('admin_menu', 'remove_menus'); }

// Remove submenu items from admin dashboard for non Admins
function remove_submenus() {
  $page = remove_submenu_page('index.php?page=relevanssi', 'relevanssi.php');;
}
add_action('admin_menu', 'remove_submenus', 999);

// Remove Yoast SEO meta box from edit posts
function remove_seo_meta() {
  remove_meta_box('wpseo_meta', 'post', 'normal');
  remove_meta_box('wpseo_meta', 'page', 'normal');
  remove_meta_box('wpseo_meta', 'trainings', 'normal');
  remove_meta_box('wpseo_meta', 'resources', 'normal');
  remove_meta_box('wpseo_meta', 'training_questions', 'normal');
  remove_meta_box('wpseo_meta', 'staff', 'normal');
  remove_meta_box('wpseo_meta', 'audits', 'normal');
}
if ( !current_user_can('create_users') ) add_action('add_meta_boxes', 'remove_seo_meta', 99);

// Add custom taxonomies and custom post types counts to dashboard
add_action( 'dashboard_glance_items', 'my_add_cpt_to_dashboard' );
function my_add_cpt_to_dashboard() {
  $showTaxonomies = 0;
  // Custom taxonomies counts
  if ($showTaxonomies) {
    $taxonomies = get_taxonomies( array( '_builtin' => false ), 'objects' );
    foreach ( $taxonomies as $taxonomy ) {
      $num_terms  = wp_count_terms( $taxonomy->name );
      $num = number_format_i18n( $num_terms );
      $text = _n( $taxonomy->labels->singular_name, $taxonomy->labels->name, $num_terms );
      $associated_post_type = $taxonomy->object_type;
      if ( current_user_can( 'manage_categories' ) ) {
        $output = '<a href="edit-tags.php?taxonomy=' . $taxonomy->name . '&post_type=' . $associated_post_type[0] . '">' . $num . ' ' . $text .'</a>';
      }
      echo '<li class="taxonomy-count">' . $output . ' </li>';
    }
  }
  // Custom post types counts
  $post_types = get_post_types( array( '_builtin' => false ), 'objects' );
  foreach ( $post_types as $post_type ) {
    if($post_type->show_in_menu==false) {
      continue;
    }
    $num_posts = wp_count_posts( $post_type->name );
    $num = number_format_i18n( $num_posts->publish );
    $text = _n( $post_type->labels->singular_name, $post_type->labels->name, $num_posts->publish );
    if ( current_user_can( 'edit_posts' ) ) {
        $output = '<a href="edit.php?post_type=' . $post_type->name . '">' . $num . ' ' . $text . '</a>';
    }
    echo '<li class="page-count ' . $post_type->name . '-count">' . $output . '</td>';
  }
}

// Add Report a Bug / BitBucket Issue Tracking Widget to Dashboard
function issue_tracker_dashboard_widget() {

  wp_add_dashboard_widget(
   'issue-tracker-widget',         // Widget slug.
   'Submit a Support Request',     // Title.
   'issue_tracker_widget_function' // Display function.
  );
}
add_action( 'wp_dashboard_setup', 'issue_tracker_dashboard_widget' );

// Issue Tracking Widget Contents
function issue_tracker_widget_function() { ?>

  <?php if ( isset($_POST['save']) ) {
    // Get User Info
    global $current_user;
    get_currentuserinfo();

    // Get message data
    $type = $_POST['request-type'];
    $subject = '['. $type .'] '. stripslashes(trim($_POST['title']));
    $message = stripslashes(trim($_POST['content']));
    $headers = 'From: '. $current_user->user_login .' <'. $current_user->user_email .'>';

    if ( mail( 'support@thatbrightrobot.com', $subject, $message, $headers ) ) {
      echo 'Success! We\'ve received your message and we\'ll get back to you as soon as possible.';
    } else {
      echo 'Sorry, there was a problem sending your message. Try emailing support@thatbrightrobot.com or calling us at (303) 832-2384.';
    }

  } else { ?>

    <p>Having trouble editing the site? Found a bug that needs to be fixed? Let us know and we'll make sure you're taken care of.</p>

    <form name="issue-tracker" action="<?php echo admin_url(); ?>" method="post" class="initial-form hide-if-no-js">

      <div class="select-wrap" id="select-wrap">
        <label for="request-type" class="prompt">What type of request is this?</label>
        <select name="request-type" id="request-type">
          <option value="HELP">Help Request</option>
          <option value="BUG">Bug Fix</option>
        </select>
      </div><p />

      <div class="input-text-wrap" id="title-wrap">
        <label class="prompt" for="title" id="title-prompt-text">Subject</label>
        <input type="text" name="title" id="title" autocomplete="off">
      </div>

      <div class="textarea-wrap" id="description-wrap">
        <label class="prompt" for="content" id="content-prompt-text">What’s the problem?</label>
        <textarea name="content" id="content" class="mceEditor" rows="3" cols="15" autocomplete="off" style="overflow: hidden; height: 33px; resize: vertical;"></textarea>
      </div>

      <p class="submit">
        <input type="submit" name="save" id="save-post" class="button button-primary" value="Send Issue">
        <br class="clear">
      </p>

    </form>

  <?php } ?>

<?php }

// Remove unecessary dashboard widgets
function example_remove_dashboard_widget() {
  remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
  remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
  remove_meta_box( 'dashboard_browser_nag', 'dashboard', 'normal' );
  remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );
}

// Hook into the 'wp_dashboard_setup' action to register our function
add_action('wp_dashboard_setup', 'example_remove_dashboard_widget' );

/**
 * TinyMCE Editor Customizations
 */

// Add buttons that are normally disabled
function my_mce_buttons_2($buttons) {
  $buttons[] = 'superscript';
  $buttons[] = 'subscript';

  return $buttons;
}
add_filter('mce_buttons_2', 'my_mce_buttons_2');

// Remove styles from the visual editor
function remove_mce_styles($arr) {
  // First check which screen
  $curr_screen = get_current_screen();
  if ( $curr_screen->post_type !== 'resources' ) {
    $arr['block_formats'] = 'Paragraph=p;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6';
    return $arr;
  } else {
    $arr['block_formats'] = 'Paragraph=p;Heading 1=h1;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6';
    return $arr;
  }
}
add_filter('tiny_mce_before_init', 'remove_mce_styles');

// Remove post types from Link to Existing Content popup
function remove_existing_content_types($query) {
  $pt_new = array();
  $exclude_types = array('training_questions', 'staff', 'audits');

  foreach ($query['post_type'] as $pt) {
    if (in_array($pt, $exclude_types)) continue;
    $pt_new[] = $pt;
  }

  $query['post_type'] = $pt_new;
  return $query;
}
add_filter('wp_link_query_args', 'remove_existing_content_types');

/**
 * THEME FUNCTIONS
 */

// Function to get correct top level parents of a page
function ehs_get_page_parents( $id = 0 ) {
  if ( $id === 0 ) $id = get_the_ID();

  $parents = array_reverse( get_post_ancestors($id) );
  $top_page = ( sizeof( $parents ) > 0 ? $parents[0] : $id );
  $values = array();

  foreach ( $parents as $parent ) {
    $value = array(
      'title' => get_the_title( $parent ),
      'url' => get_the_permalink( $parent ),
      'id' => $parent
    );

    array_push($values, $value);
  }
  return array(
    'parents' => $values,
    'top' => $top_page
  );
}

// Email settings page
function theme_email_settings() {
  if ( !current_user_can('create_users') ) {
    wp_die('You do not have sufficient permissions to access this page.');
  } ?>

  <?php if ( isset($_POST['update_settings']) ) {

    $default_email = esc_attr($_POST['default_email']);
    update_option("ehs_default_email", $default_email); ?>

    <div id="message" class="updated">Settings saved.</div>

  <?php }

  $default_email = get_option("ehs_default_email"); ?>

  <div class="wrap">
    <h2>Email Form Settings</h2>
    <form method="post" action="">

      <table class="form-table">
        <tr valign="top">
          <th scope="row">
            <label for="default_email">
              Default email receipient for all forms:
            </label>
          </th>
          <td>
            <input type="text" name="default_email" size="25" value="<?php echo $default_email; ?>">
          </td>
        </tr>
      </table>

      <input type="hidden" name="update_settings" value="Y">

      <p>
        <input type="submit" value="Save settings" class="button-primary">
      </p>

    </form>
  </div>

<?php }
// Building Codes page
function theme_building_codes() {
  if (!current_user_can('edit_posts')) {
    wp_die('You do not have sufficient permissions to access this page.');
  }

  if (isset($_POST['update_settings'])) {
    $keys = $_POST['building_codes']['keys'];
    $vals = $_POST['building_codes']['vals'];
    $keysTrim = array();
    $valsTrim = array();
    foreach ($keys as $key) {
      if (!empty($key)) $keysTrim[] = $key;
    }
    foreach ($vals as $val) {
      if (!empty($val)) $valsTrim[] = $val;
    }
    $updated_codes = array_combine($keysTrim, $valsTrim);
    ksort($updated_codes);
    update_option("ehs_building_codes", $updated_codes);
    echo '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>Changes saved.</strong></p></div>';
  }

  $building_codes = get_option("ehs_building_codes");
  if (empty($building_codes)) {
    $building_codes = array(
        "ABSA" => "ABSAROKA",
        "ACAD" => "THE ACADEMY",
        "ALMG" => "ALUMNI CTR GARAGE ANNEX",
        "ALUM" => "KOENIG ALUMNI CENTER",
        "ARCE" => "ADMIN & RSCH CTR EAST CAMPUS",
        "ARCEA" => "ADMIN & RSCH CTR MECH BLDG",
        "ARMR" => "ARMORY",
        "ARMT" => "ARMORY TRAILER",
        "ASPN" => "ASPEN",
        "ATLS" => "ROSER ATLAS CENTER",
        "BATH" => "BATH HOUSE",
        "BCAT" => "BOBCAT",
        "BESC" => "BENSON EARTH SCIENCES BLDG",
        "BHRN" => "BIGHORN",
        "BIOT" => "Jennie Smoly Caruthers Biotechnology Bldg.",
        "BISN" => "BISON",
        "BLWR" => "BLOWER BUILDING",
        "BREG" => "BICYCLE REGISTRATION BUILDING",
        "C4C" => "CENTER FOR COMMUNITY",
        "CARL" => "CARLSON GYMNASIUM",
        "CARP" => "CARPENTER SHOP",
        "CAS" => "CENTER FOR ASIAN STUDIES",
        "CASA" => "CASA -CTR FOR ASTRO & SPACE ASTRNMY",
        "CEDU" => "CONTINUING EDUCATION CENTER",
        "CHEM" => "CRISTOL CHEMISTRY & BIOCHEM BLDG",
        "CHKD" => "CHICKADEE",
        "CHKR" => "CHICKAREE",
        "CINC" => "CENTER FOR INNOVATION & CREATIVITY",
        "CIRE" => "CIRES COOP INST FOR RSCH IN ENV SCI",
        "CLMB" => "COLUMBINE",
        "CLRE" => "CLARE SMALL ARTS & SCIENCES",
        "CLUB" => "UNIVERSITY CLUB",
        "COLE" => "COLEMAN INSTITUTE SPACE",
        "COMP" => "COMPUTING CENTER",
        "COTT" => "GATES WOODRUFF WOMENS STUDIES COTT",
        "COYO" => "COYOTE",
        "CPMP" => "COLORADO POND PUMP STATION",
        "DALW" => "DAL WARD ATHLETIC CENTER",
        "DDW" => "DUANE D-WING",
        "DEN" => "DENISON ARTS & SCIENCES BLDG",
        "DLC" => "DISCOVERY LEARNING CNTR",
        "DUAN" => "DUANE PHYSICS",
        "ECAD" => "ENGINEERING ADMINISTRATION WING",
        "ECAE" => "ENGINEERING AEROSPACE WING",
        "ECCE" => "ENGINEERING CIVIL & ENVIRO WING",
        "ECCR" => "ENGINEERING CLASSROOM WING",
        "ECCS" => "ENGINEERING COMP SCIENCE DEPT WING",
        "ECEE" => "ENGINEERING ELECTRICAL WING",
        "ECES" => "ENGINEERING ENVIRONMENTAL SUSTAINABILITY",
        "ECME" => "ENGINEERING MECHANICAL WING",
        "ECNT" => "ENGINEERING NORTH TOWER",
        "ECON" => "ECONOMICS BLDG",
        "ECOT" => "ENGINEERING OFFICE TOWER",
        "ECPDC" => "EAST CAMPUS POWER DISTRIBUTION CENTER",
        "ECSL" => "ENGINEERING STORES & LABS",
        "ECST" => "ENGINEERING SOUTH TOWER",
        "EDEP" => "EAST DISTRICT ENERGY PLANT",
        "EDUC" => "EDUCATION BLDG",
        "EHSC" => "ENVIRONMENTAL HEALTH & SAFETY CNTR",
        "EKLC" => "EKELEY SCIENCES BLDG",
        "ELSH" => "ELECTRIC SHOP",
        "ENVD" => "ENVIRONMENTAL DESIGN BLDG",
        "EPRK" => "EUCLID AV AUTOPARK",
        "EVNT" => "COORS EVENTS/CONFERENCE CENTER",
        "FH" => "BALCH FIELDHOUSE COMPLEX",
        "FHPB" => "BALCH FIELDHOUSE PRESSBOX",
        "FISK" => "FISKE PLANETARIUM & SCI CENTER",
        "FLMG" => "FLEMING BUILDING",
        "FLYC" => "FLYCATCHER",
        "FNCH" => "FINCH",
        "FPA" => "A&S FINANCE AND PAYROLL ADMINISTRATION",
        "FRTL" => "FIRE TOOLS",
        "GASR" => "GAS REGULATOR BUILDING",
        "GH-1" => "GREENHOUSE NO 1 AT MACKY",
        "GH-3" => "RESEARCH PARK GREENHOUSE",
        "GIPF" => "1060 REGENT DRIVE",
        "GNSH" => "GENERATOR SHED",
        "GOLD" => "GOLD BIOSCIENCES BUILDING",
        "GRGE" => "GARAGE",
        "GROU" => "GROUSE",
        "GRSH" => "GARAGE SHED",
        "GSHK" => "GOSHAWK",
        "GUGG" => "GUGGENHEIM GEOGRAPHY BLDG",
        "HALE" => "HALE SCIENCE BLDG",
        "HAZM" => "HAZMAT SHED",
        "HEND" => "HENDERSON BLDG (MUSEUM)",
        "HFOC" => "Housing & Dining Services Facilities Operations Ce",
        "HLMS" => "HELLEMS ARTS & SCIENCES BLDG",
        "HPCF" => "HIGH PERFORMANCE COMPUTING FACILITY",
        "HSSC" => "HOUSING SYS SERVICE CENTER",
        "HUMM" => "HUMMINGBIRD",
        "HUMN" => "EATON HUMANITIES BLDG",
        "IBG" => "INST FOR BEHAVIORAL GENETICS",
        "IBS" => "INSTITUTE OF BEHAVIORAL SCIENCE",
        "IBS2" => "INST OF BEHAV SCI NO 2",
        "ITLL" => "DRESCHER UNDERGRADUATE ENGINEERING",
        "JAY" => "JAY",
        "JILA" => "JILA (JOINT INST FOR LAB ASTROPHYSICS)",
        "JNCO" => "JUNCO",
        "KCEN" => "KITTREDGE CENTRAL",
        "KCHN" => "KITCHEN",
        "KCNC" => "KITCHEN COOLER",
        "KIOW" => "KIOWA LAB",
        "KOBL" => "KOELBEL BUILDING - LEEDS SCH OF BUS",
        "KTCH" => "KETCHUM ARTS & SCIENCES BLDG",
        "KVCU" => "KVCU RADIO TOWER",
        "LESS" => "LESSER HOUSE",
        "LIBR" => "NORLIN LIBRARY",
        "LITR" => "LITMAN RESEARCH LAB (RL-1)",
        "LMBR" => "LIMBER",
        "LSRL" => "LIFE SCI RESEARCH LAB (RL-4)",
        "LSTR" => "LASP SPACE TECHNOLOGY RSCH CTR",
        "LTRN" => "LATRINE",
        "MAC" => "MACALLISTER BUILDING",
        "MAIN" => "OLD MAIN",
        "MARR" => "MARR ALPINE LABORATORY",
        "MATH" => "MATHEMATICS BUILDING",
        "MCFL" => "MOORES COLLINS FAMILY LODGE",
        "MCKY" => "MACKY AUDITORIUM",
        "MCOL" => "BRUCE CURTIS BLDG",
        "MGRN" => "MEGARON CLASSROOM",
        "MKNA" => "MCKENNA LANGUAGES BLDG",
        "MRCTM" => "MARINE COURT MAIL",
        "MRMT" => "MARMOT",
        "MRTN" => "MERTENSIA",
        "MSSC" => "MARINE ST SCIENCE CTR (RL-6)",
        "MUEN" => "MUENZINGER PSYCH & BIOPSYCH",
        "MUS" => "IMIG MUSIC BLDG",
        "NTCR" => "NUTCRACKER",
        "OB1" => "ARTS AND SCIENCES OFFICE BUILDING 1",
        "OBSR" => "OBSERVATORY",
        "OBSV" => "SOMMERS-BAUSCH OBSERVATORY",
        "PDLP" => "PONDEROSA LODGEPOLE",
        "PDPS" => "POLICE & PARKING SERVICES CTR",
        "PFDC" => "PAGE FOUNDATION CENTER",
        "PGRG" => "PONDEROSA GARAGE",
        "PIKA" => "PIKA",
        "PORC" => "PORCUPINE",
        "PORT" => "PORTER BIOSCIENCES",
        "POWR" => "POWER HOUSE",
        "PTRM" => "PTARMIGAN",
        "RAMY" => "RAMALEY BIOLOGY BLDG",
        "REC" => "STUDENT RECREATION CENTER",
        "RGNT" => "REGENT ADMINISTRATIVE CENTER",
        "RL2" => "RESEARCH LAB NO 2",
        "RL6" => "RESEARCH LAB NO 6",
        "ROBN" => "ROBIN",
        "RPMP" => "RSCH PARK PUMP STATION",
        "RPRK" => "REGENT DR AUTOPARK",
        "SEEW" => "4003 DISCOVERY DRIVE",
        "SLHS" => "COMMUNICATION DISORDERS",
        "SLL" => "SCIENCE LEARNING LABORATORY",
        "SPRG" => "SPRING HOUSE",
        "SPSC" => "SPACE SCIENCE BUILDING (formerly 3665 Disc. Dr.)",
        "SPSK" => "SAPSUCKER",
        "SSKN" => "SISKEN",
        "STAD" => "STADIUM BUILDING",
        "STBK" => "1402 BROADWAY",
        "STSB" => "STADIUM SKY BOX",
        "STTB" => "STADIUM TICKET BUILDING",
        "SUMM" => "SUMMER",
        "TB01" => "TEMP BLDG 1 AT CLARE SMALL",
        "TB15" => "ELECTRIC SUPPLY BLDG",
        "TB16" => "TELECOM EQUIP BLDG, SMILEY CT",
        "TB19" => "UNIV ADMIN CTR ANNEX",
        "TB45" => "WAREHOUSE NO 1",
        "TB46" => "WAREHOUSE NO 2",
        "TB47" => "WAREHOUSE NO 3",
        "TB48" => "WAREHOUSE NO 4",
        "TB49" => "STEAM CONVERSION SHED",
        "TB65" => "TEMPORARY BLDG 65",
        "TB65A" => "TEMPORARY BLDG 65 GARAGE",
        "TB68" => "FAM HOUSING COMMUNITY CENTER",
        "TB81" => "GATEHOUSE/INFO BOOTH",
        "TB83" => "POTTS FIELD TRACK STORAGE",
        "TB84" => "BRYAN BENJAMIN SAX SKI TEAM BLDG",
        "TB85" => "AUX TRACK STORAGE, NW POTTS FLD",
        "TETN" => "TETON",
        "THTR" => "UNIVERSITY THEATRE",
        "TLC" => "TECHNOLOGY LEARNING CENTER",
        "TLRS" => "TULAROOSA",
        "TNDR" => "TUNDRA LAB",
        "TRAN" => "TRANSPORTATION CENTER AND ANNEX",
        "UCTR" => "UNIVERSITY ADMIN CTR",
        "UMC" => "UNIVERSITY MEMORIAL CENTER",
        "UNIT" => "UNITA",
        "VAC" => "VISUAL ARTS COMPLEX",
        "VHCL" => "VEHICLE STORAGE",
        "VPMP" => "VARSITY LAKE PUMP STATION",
        "WALN" => "WALNUT DISTRIBUTION CENTER",
        "WARD" => "WARDENBURG STUDENT HEALTH CTR",
        "WASA" => "WASATCH",
        "WDBY" => "WOODBURY ARTS & SCIENCES BLDG",
        "WILD" => "2860 WILDERNESS PLACE",
        "WLAW" => "WOLF LAW BUILDING",
        "WLLW" => "WILLOW",
        "WPTI" => "WAPATI",
        "WRVR" => "WIND RIVER",
        "WSTE" => "WASTE TREATMENT",
        "WVHP" => "WILLIAMS VILL HEATING PLANT",
        "WVN" => "WILLIAMS VILLAGE NORTH",
        "WVPH" => "WILLIAMS VILLAGE PUMPHOUSE",
        "WVSTO" => "WILLIAMS VILLAGE STORAGE",
        "XCUF" => "UNIVERSITY OF COLORADO FOUNDATION",
        "ZUNI" => "ZUNI"
    );
  } // end default conditional ?>

  <div class="wrap">
    <h2>Manage Building Codes List</h2>
    <form method="post" action="">

      <table class="form-table">
        <tr valign="top">
          <th scope="row">
            <h3 style="margin: 0;">Code</h3>
          </th>
          <th scope="row">
            <h3 style="margin: 0;">Full Name</h3>
          </th>
        </tr>
        <tr valign="top">
          <td scope="row" style="width: 25%; padding: 0 5px;">
            <input type="text" name="building_codes[keys][0]" style="width: 100%;" placeholder="New Building Code">
          </td>
          <td scope="row" style="width: 75%; padding: 0 5px;">
            <input type="text" name="building_codes[vals][0]" style="width: 100%;" placeholder="New Building Name">
          </td>
        </tr>
        <?php
        $count = 1;
        foreach ($building_codes as $code => $name) { ?>
          <tr valign="top">
            <td scope="row" style="width: 25%; padding: 0 5px;">
              <input type="text" name="<?php echo 'building_codes[keys]['. $count .']' ?>" value="<?php echo $code; ?>" style="width: 100%;">
            </td>
            <td scope="row" style="width: 75%; padding: 0 5px;">
              <input type="text" name="<?php echo 'building_codes[vals]['. $count .']' ?>" value="<?php echo $name; ?>" style="width: 100%;">
            </td>
          </tr>
        <?php $count++;
        } ?>
      </table>

      <input type="hidden" name="update_settings" value="Y">

      <p>
        <input type="submit" value="Update" class="button-primary">
      </p>

    </form>
  </div>

<?php }

// Add them to the menu
function setup_theme_admin_menus() {
  // Let's add a submenu
  /*
  add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
  add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function );
   */
  add_menu_page('Email Settings', 'Email Settings', 'create_users',
    'ehs-site-settings', 'theme_email_settings',
    get_bloginfo('template_url').'/img/cu-logo-wp.png');

  add_menu_page('Building Codes', 'Building Codes', 'edit_posts',
    'ehs-building-codes', 'theme_building_codes',
    get_bloginfo('template_url').'/img/cu-logo-wp.png');
}
add_action('admin_menu', 'setup_theme_admin_menus');

// Backend Favicon
add_action('admin_head', 'show_favicon');
function show_favicon() {
  echo '<link href="' . get_bloginfo('template_url') . '/img/cu-logo.png" rel="icon" type="image/x-icon">';
}

// Function to return file size
function file_size($url){
    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, TRUE);
    curl_setopt($ch, CURLOPT_NOBODY, TRUE);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

    $data = curl_exec($ch);
    $bytes = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);

    curl_close($ch);

    // $bytes is filesize in bytes, now convert it to a human readable format.

    $bytes = floatval($bytes);
        $arBytes = array(
            0 => array(
                "UNIT" => "TB",
                "VALUE" => pow(1024, 4)
            ),
            1 => array(
                "UNIT" => "GB",
                "VALUE" => pow(1024, 3)
            ),
            2 => array(
                "UNIT" => "MB",
                "VALUE" => pow(1024, 2)
            ),
            3 => array(
                "UNIT" => "KB",
                "VALUE" => 1024
            ),
            4 => array(
                "UNIT" => "B",
                "VALUE" => 1
            ),
        );

    foreach($arBytes as $arItem)
    {
        if($bytes >= $arItem["VALUE"])
        {
            $result = $bytes / $arItem["VALUE"];
            $result = str_replace(".", "." , strval(round($result, 2)))." ".$arItem["UNIT"];
            break;
        }
    }
    return $result;
}

/* 
replacing the default "Enter title here" placeholder text in the title input box
with something more descriptive can be helpful for custom post types 
 
place this code in your theme's functions.php or relevant file
 
source: http://flashingcursor.com/wordpress/change-the-enter-title-here-text-in-wordpress-963
*/
 
function change_default_title( $title ){
 
    $screen = get_current_screen();
 
    if ( 'training_questions' == $screen->post_type ){
        $title = 'Enter question here';
    }

    if ( 'staff' == $screen->post_type ) {
      $title = 'Last, First';
    }
 
    return $title;
}
 
add_filter( 'enter_title_here', 'change_default_title' );

/**
 * Rename Posts to News and Events
 */
function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'News & Updates';
    $submenu['edit.php'][5][0] = 'News & Updates';
    $submenu['edit.php'][10][0] = 'Add News';
    $submenu['edit.php'][16][0] = 'News Tags';
    echo '';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'News';
    $labels->singular_name = 'News';
    $labels->add_new = 'Add News';
    $labels->add_new_item = 'Add News';
    $labels->edit_item = 'Edit News';
    $labels->new_item = 'News';
    $labels->view_item = 'View News';
    $labels->search_items = 'Search News';
    $labels->not_found = 'No News found';
    $labels->not_found_in_trash = 'No News found in Trash';
    $labels->all_items = 'All News';
    $labels->menu_name = 'News';
    $labels->name_admin_bar = 'News';
}
 
add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );

/**
 * Remove Custom Post Type from Archive.php
 */

function exclude_cpt( $query ) {
  if ( $query->is_tax('departments') && is_archive() ) {
    $query->set( 'post_type', 'post' );
  }
}
add_action('pre_get_posts', 'exclude_cpt');