<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?php if ( is_front_page() ) { bloginfo('name'); } else { wp_title(' | ', true, 'right'); bloginfo('name'); } ?> | CU-Boulder
    </title>
    <?php wp_head(); ?>
    <link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_url'); ?>/img/favicon.ico">
    <!--[if lt IE 9]>
    <style>.hero .cta-search .module{background:black !important;} .search-submit{background-image:url('../img/search.png');background-size:90%;background-position:center;background-repeat:no-repeat;}</style>
    <script src="<?php bloginfo('template_url'); ?>/js/html5shiv.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(); ?>>

    <?php get_template_part('search','scraper'); ?>

    <div class="header grid">
        
        <div class="header-banner left">
            
            <img src="<?php bloginfo('template_url'); ?>/img/cu-logo.svg" class="cu-logo left">

            <h1 class="site-title"><?php bloginfo('name'); ?></h1>
            <h2 class="site-desc"><?php bloginfo('description'); ?></h2>

        </div>

        <div class="cu-search right">

            <a href="http://www.colorado.edu/" target="_blank"><strong>CU:</strong> Home</a> &middot;
            <a href="http://www.colorado.edu/atoz" target="_blank">A to Z</a> &middot;
            <a href="http://www.colorado.edu/campusmap" target="_blank">Campus Map</a><br />
            
            <?php get_search_form(); ?>

        </div>
        <div class="cf"></div>

    </div>

    <?php get_template_part('hero'); ?>