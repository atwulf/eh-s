<?php // Get background image for hero
if ( get_field('banner_image') ) { # Default to defined banner image
    $banner_image = get_field('banner_image');

} elseif ( get_post_type() == 'trainings' ) { # Training CPT should pull frm Trainings page
    $training = get_page_by_title('Training');
    $banner_image = get_field('banner_image', $training);

} elseif ( get_post_type() == 'resources' ) { # Resources CPT should pull frmo Resurces page
    $resources = get_page_by_title('Resources');
    $banner_image = get_field('banner_image', $resources);

} elseif ( get_post_type() == 'post' ) {
    $about = get_page_by_title('About');
    $banner_image = get_field('banner_image', $about);

} elseif ( is_404() ) { # 404.php
    $banner_image = get_bloginfo('template_url') . '/img/lab-safety.jpg';

} else { # If banner image is not defined, inherit

    $parents = ehs_get_page_parents();
    $parents = $parents['top'];
    $top_parent = get_field('banner_image', $parents);
    $immed_parent = get_field('banner_image', $post->post_parent);

    // Default to immediate parent's banner image if it's defined
    $banner_image = ( $immed_parent ? $immed_parent : $top_parent );

}
?>

<div class="hero" style="<?php echo 'background-image:url(\''. $banner_image .'\');'; ?>">

    <?php // Call Nav Menu
        $args = array(
            'theme_location' => 'main-menu',
            'container' => 'ul',
            'container_class' => 'menu'
        );

        wp_nav_menu($args);
    ?>
    
    <?php if ( is_front_page() ) { ?>
    <div class="grid">
        <div class="wrapper">
            
            <div class="col-1-2">

    <?php } ?>
                <div class="cta-search">

                    <div class="module">

                        <span class="cta-header">Find the right resource when you need it.</span>
                    
                        <?php get_search_form(); ?>


                    </div>

                </div>
    <?php if ( is_front_page() ) { ?>
            </div>

        </div>
    </div>
    <?php } ?>

</div>