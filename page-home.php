<?php get_header(); ?>

    <div class="content wrapper">

        <div class="grid sub-cta-home">

            <div class="col-3-4">
                <div class="grid">

                    <div class="col-1-2">
                        <div class="module">

                            <h3>Researchers &amp; Laboratory Contacts</h3>
                            <p>Find safety procedures. Learn about and schedule required trainings for your position.</p>
                            <a href="<?php echo get_the_permalink( get_page_by_title('Lab Support') ); ?>" class="button-gold">Find Information</a>

                        </div>
                    </div>

                    <div class="col-1-2">
                        <div class="module">

                            <h3>Construction &amp; Facilities Management</h3>
                            <p>Learn about safety requirements on campus. Stay compliant by learning about guidelines for your current project.</p>
                            <a href="<?php echo get_the_permalink( get_page_by_title('Campus Support') ); ?>" class="button-gold">Find Information</a>

                        </div>
                    </div>

                </div>
            </div>

            <div class="col-1-4 emergency-home">
                <div class="module">

                    <h3>Questions and Concerns</h3>
                    <p>If you have a life-threatening emergency, please call 911.</p>
                    <p>If your issue is not an emergency and you would like to report it to EH&amp;S for assistance, please click the button below.</p>
                    <a href="https://ehsonline.colorado.edu/Asbestos/Asbestosdb/Account/Login.aspx?user=contact" target="_blank" class="button-red">Submit a Concern</a>

                </div>

                <?php /*
                <div class="module">

                    <div class="emergency-icon"></div>
                    <span>In emergencies, call 911 first.</span>

                </div>

                */ ?>
            </div>

        </div>

        <div class="block-800">
            <div class="grid">

                <div class="col-1-2">

                    <?php if(have_posts()) : while(have_posts()) : the_post(); ?>

                    <?php the_content(); ?>

                    <?php endwhile;endif; ?>

                </div>

                <div class="col-1-2">
                    <div class="module">

                        <h4>News and Updates</h4>

                        <?php
                        $latest_blog = new WP_Query('post_type=post&posts_per_page=2');

                        if ( $latest_blog->have_posts() ) { ?>
                            <ul class="home-loop">
                        <?php while ( $latest_blog->have_posts() ) { 
                                $latest_blog->the_post(); ?>

                                <?php get_template_part('entry'); ?>

                        <?php } ?>
                                <a href="<?php echo esc_url(home_url('/news-and-updates')); ?>" class="button-gold">See All News</a>
                            </ul>
                        <?php } else {
                            echo '<p>There are currently no News & Updates posts to show.</p>';
                        }
                        wp_reset_postdata(); ?>

                    </div>
                </div>

            </div>
        </div>

    </div>

<?php get_footer(); ?>