<?php /*
Template Name: Password Protected
 */ ?>

<?php get_header(); ?>

<div class="content wrapper">

    <?php get_template_part('breadcrumbs'); ?>

    <?php // Check if user is logged in and has permissions
    if ( ! is_user_logged_in() || ! current_user_can('edit_post') ) {
        $parents = ehs_get_page_parents( get_the_ID() );
        $top_page = get_page($parents['top']); ?>

        <h1><?php echo $top_page->post_title; ?></h1>

        <div class="grid">

            <?php get_sidebar(); ?>

            <div class="main col-2-3">

                <h2>Access Denied</h2>

                <p>Sorry, you must be logged in to view this page.</p>
                
            </div>
            
        </div>

    <?php } else { ?>
    
        <?php if ( have_posts() ) {

            while ( have_posts() ) {

                the_post();
                get_template_part('entry');

            }

        }
    } ?>

</div>

<?php get_footer(); ?>