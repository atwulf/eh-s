<?php get_header(); ?>

<div class="content wrapper">

    <?php get_template_part('breadcrumbs'); ?>

    <h1>Resources</h1>

    <div class="grid">
        
        <?php get_sidebar(); ?>

        <div class="main col-2-3">
        
            <?php if ( isset( $_GET ) ) {
                $filters = array(
                    'keyword' => $_GET['kw'],
                    'date' => $_GET['date'],
                    'concern' => $_GET['con'],
                    'type' => $_GET['type'],
                    'unit' => $_GET['unit'],
                    'sort' => $_GET['sortby'],
                    'order' => $_GET['order']
                );

                foreach ( $filters as $key => $val ) {
                    if ( $val === 'all' ) $filters[$key] = '';
                }
            } ?>
            
            <div class="resources-filters">

                <?php // Maintain existing query string
                $query = '?';
                $sort = 'title';
                $order = 'ASC';

                if ( isset ($_GET) ) {
                    foreach ( $_GET as $key => $val ) {
                        $val = str_replace(' ','+', $val);
                        if ( $key == 'sortby' ) $sort = $val;
                        if ( $key == 'order' ) $order = $val;
                        if ( $key !== 'sortby' && $key !== 'order' ) $query .= $key .'='. $val .'&';
                    }
                } ?>

                <div class="grid">
                    <div class="col-1-2">
                        <label><span>Sort by</span></label>
                        <a href="<?php echo $query .'sortby=title&order='. $order; ?>" class="button-secondary <?php if ($filters['sort'] == 'title' || !isset($filters['sort'])) echo 'active'; ?>">Title</a>
                        <a href="<?php echo $query .'sortby=date&order='. $order; ?>" class="button-secondary <?php if ($filters['sort'] == 'date') echo 'active'; ?>">Last Update</a>
                    </div>
                    <div class="col-1-2">
                        <label><span>Order</span></label>
                        <a href="<?php echo $query . 'sortby='. $sort .'&order=ASC'; ?>" class="button-secondary <?php if ($filters['order'] == 'ASC' || !isset($filters['order'])) echo 'active'; ?>">
                            <?php if ( $sort === 'title' ) {
                                echo 'A to Z';
                            } else {
                                echo 'Oldest';
                            } ?>
                        </a>
                        <a href="<?php echo $query .'sortby='. $sort .'&order=DESC'; ?>" class="button-secondary <?php if ($filters['order'] == 'DESC') echo 'active'; ?>">
                            <?php if ( $sort == 'title' ) {
                                echo 'Z to A';
                            } else {
                                echo 'Newest';
                            } ?>
                        </a>
                    </div>
                </div>
                
            </div>

            <?php // Create Resources CPT Loop
            $posts_per_page = -1;

            if ( isset($filters['keyword']) && $filters['keyword'] !== '' ) { # Relevanssi Filters
                // Accepts same arguments as WP_Query
                $args = array(
                    'post_type' => 'resources',
                    'posts_per_page' => -1
                );

                // Sort order
                $args['order'] = $order;
                $args['orderby'] = $sort;

                $resources_loop = new WP_Query($args);

                // Basic Search bits
                $resources_loop->query_vars['post_type'] = 'resources';
                $resources_loop->query_vars['s'] = $filters['keyword'];
                $resources_loop->query_vars['posts_per_page'] = $posts_per_page;

                // Document Concern
                if ( $filters['concern'] !== '' ||
                     $filters['type'] !== '' ||
                     $filters['unit'] !== '' )
                {
                    $resources_loop->query_vars['tax_query']['relation'] = 'AND';
                }
                if ( $filters['concern'] !== '' ) :
                    array_push($resources_loop->query_vars['tax_query'],
                        array(
                            'taxonomy' => 'doc_concerns',
                            'field' => 'slug',
                            'terms' => array( $filters['concern'] )
                        )
                    );
                endif;

                // Document Type
                if ( $filters['type'] !== '' ) :
                    array_push($resources_loop->query_vars['tax_query'],
                        array(
                            'taxonomy' => 'doc_type',
                            'field' => 'slug',
                            'terms' => array( $filters['type'] )
                        )
                    );
                endif;

                // Department
                if ( $filters['unit'] !== '' ) :
                    array_push($resources_loop->query_vars['tax_query'],
                        array(
                            'taxonomy' => 'departments',
                            'field' => 'slug',
                            'terms' => array( $filters['unit'] )
                        )
                    );
                endif;

                // Last Update
                if ( isset( $filters['date'] ) && $filters['date'] !== 'all' ) {
                    $date = $filters['date'];

                    if ( $date === 'last-30-days' ) {
                        $date = '30 days ago';
                    } elseif ( $date === 'last-6-months' ) {
                        $date = '6 months ago';
                    } elseif ( $date === 'last-year' ) {
                        $date = '1 year ago';
                    } else {
                        $date = 'today';
                    }

                    $resources_loop->query_vars['date_query'] = array(
                        array(
                            'column' => 'post_date_gmt',
                            'before' => $date
                        )
                    );
                }
                relevanssi_do_query($resources_loop);

            } else { # List all resources, no search term (default view)

                $args = array(
                    'post_type' => 'resources',
                    'posts_per_page' => $posts_per_page
                );

                // Sort order
                $args['order'] = $order;
                $args['orderby'] = $sort;

                // Document Concern
                if ( $filters['concern'] !== '' ||
                     $filters['type'] !== '' ||
                     $filters['unit'] !== '' )
                {
                    $args['tax_query']['relation'] = 'AND';
                }
                if ( isset($filters['concern']) && $filters['concern'] !== '' ) :
                    array_push($args['tax_query'],
                        array(
                            'taxonomy' => 'doc_concerns',
                            'field' => 'slug',
                            'terms' => array( $filters['concern'] )
                        )
                    );
                endif;

                // Document Type
                if ( isset($filters['type']) && $filters['type'] !== '' ) :
                    array_push($args['tax_query'],
                        array(
                            'taxonomy' => 'doc_type',
                            'field' => 'slug',
                            'terms' => array( $filters['type'] )
                        )
                    );
                endif;

                // Department
                if ( isset($filters['unit']) && $filters['unit'] !== '' ) :
                    array_push($args['tax_query'],
                        array(
                            'taxonomy' => 'departments',
                            'field' => 'slug',
                            'terms' => array( $filters['unit'] )
                        )
                    );
                endif;

                // Last Update
                if ( isset( $filters['date'] ) && $filters['date'] !== 'all' ) {
                    $date = $filters['date'];

                    if ( $date === 'last-30-days' ) {
                        $date = '30 days ago';
                    } elseif ( $date === 'last-6-months' ) {
                        $date = '6 months ago';
                    } elseif ( $date === 'last-year' ) {
                        $date = '1 year ago';
                    } else {
                        $date = 'today';
                    }

                    $args['date_query'] = array(
                        array(
                            'column' => 'post_date_gmt',
                            'before' => $date
                        )
                    );
                }

                $resources_loop = new WP_Query($args);
            }

            if ( $resources_loop->have_posts() ) {
                echo '<div class="resources-list">'; ?>

                <div class="grid">
                    <div class="col-1-2">
                        <strong>Title</strong>
                    </div>
                    <div class="col-1-4">
                        <strong>Last Update</strong>
                    </div>
                    <div class="col-1-4">
                        <strong>Unit</strong>
                    </div>
                </div>

                <?php while ( $resources_loop->have_posts() ) {
                    $resources_loop->the_post(); ?>
                    
                    <?php get_template_part('entry'); ?>
                
                <?php } // while
                echo '</div>';
            } else {
                echo '<p>Sorry, nothing was found for your search.</p><p>Try loosening your filters or using a synonym for one of your keywords.</p>';
            } // if
            ?>

        </div>

    </div>

</div>

<?php get_footer(); ?>