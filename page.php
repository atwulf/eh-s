<?php get_header(); ?>

<div class="content wrapper">

    <?php get_template_part('breadcrumbs'); ?>
    
    <?php if ( have_posts() ) {

        while ( have_posts() ) {

            the_post();
            get_template_part('entry');

        }

    } ?>

</div>

<?php get_footer(); ?>