# WordPress Theme for CU-Boulder Environmental Health and Safety

##### A Bright Robot Project

This is a gulp project. Just type `gulp` in the command line and you're good. It should open the local dev site, stage site, live site, SourceTree project, and an additional Terminal window in the CWD, since the one you ran gulp from is now running `gulp watch` on your styles and scripts.

Nice and easy startup. (Do `gulp restart` to skip the startup stuff and go straight to recompile + watch.)

### EH&S Theme Guide

Documentation related to managing the WordPress side of things is available at [ehs.colorado.edu/help](http://ehs.colorado.edu/help).

The theme is set up to be as modular and flexible as possible without losing the beauty of the overall template or the content organization and architecture we designed.

To that end many of the templates in this theme are partials. The `templates/` subfolder contains many of the smaller pieces of the site, including single/loop views for custom post types, contact forms, and sidebars.

The CSS class name selectors and rules are written accordingly as well. This site is the first Bright Robot site to attempt to avoid content-aware CSS rules. Please keep this in mind when maintaining this site.

### Change Log

##### December 9

- Issues resolved: [#8](https://bitbucket.org/atwulf/eh-s/issue/8/fix-staff-cpt-loop-list-order), [#7](https://bitbucket.org/atwulf/eh-s/issue/7/allow-multiple-email-recipients), [#18](https://bitbucket.org/atwulf/eh-s/issue/18/change-building-code-field-to-a-dropdown)

##### November 12
- Issues resolved: [#6](https://bitbucket.org/atwulf/eh-s/issue/6/write-documentation), [#14](https://bitbucket.org/atwulf/eh-s/issue/14/change-website-issue-form-to-ask-a), [#4](https://bitbucket.org/atwulf/eh-s/issue/4/broken-old-links-in-trainings)
- Changed "Report a Website Issue" to "Submit a Support Request"
- Added "request type" select to Support Request form

##### November 11
- Issues resolved: [#11](https://bitbucket.org/atwulf/eh-s/issue/11/link-hazmat-and-rad-training-forms-from), [#9](https://bitbucket.org/atwulf/eh-s/issue/9/trainings-offsite-link-custom-field-doesnt), [#12](9), [#13](https://bitbucket.org/atwulf/eh-s/issue/13/add-a-second-cta-take-quiz-to-trainings)
- Changed how forms on Trainings posts are handled
- Changed how WordPress assembles Trainings forms

##### November 6
Whew. Site is live at http://ehs.colorado.edu as of yesterday. Today, we're putting out fires. I'm glad we're launching on Wednesdays now.

Basically what's happening is, we launched and set all URIs under colorado.edu/ehs to redirect to the new domain's home page.

Bad idea.

It was way too jarring for people who more than likely landed there from a bookmark or from Google expecting to get what they were looking for and instead got a home page telling them to search.

And search, initially, did not deliver. Today started with watching people land on SERPs that didn't point them to the right page. As the day went on though SERPs seemed to improve, and I remembered Relvanssi supports custom synonyms and that improved SERPs for a couple more common search terms, which was great.

Further fires to put out include a better roll out plan to start moving people to the new site. Also, the Radiation server is fucked up and they think it's our fault maybe? I can't tell if that fire's out yet.

Minor bugfixes to be completed this week will be added to the issue tracker, which should also be the main point of reference for work from here on out on this website.

##### Oct 7
Yesterday would have been the site launch. Tomorrow we present the final stage site to the management team. Then bug testing will commence, to include:

- finalization of responsive design
- email forms hookup
- CSS finalization & shipping prep
- final bug testing

This update has been primarily maintenance as we've added new pieces of content to the site. Notably, we redesigned the search results page, paginated the Resources list with jQuery, and **FANFARE** gave the site an average load time of 1.05s! Great job!

##### Sept 24
Today was originally meant to be the final stage site, but due to content issues, we're having to push the project completion date back a week. As of this repo update, the largest chunks of functionality are completed.

Adding content to the staging site has helped edify things like parent/child inheritance for things like the Banner Image and the More Information sidebar panel.

Major points in this update:
- Search now uses Relvanssi for search engine
- New baked-in form: Radiation Waste Pickup
- Header styles
- Resources CPT now allows for File-based resources (PDFs, etc) and link-based resources (off-site links with preamble, probably won't use this one)

Some thing still to come:
- Hook up and complete all contact forms
- Responsive (full site)
- Finalize SERP design
- Fill-in functionality as needed with content

There are so many Resources and Trainings to put in, it's overwhelming. Our new Final Stage Site Date is October 8th(ish) when we'll have a meeting with the managers and Derrick and present the "final site" and finalize post-project details. Then bug testing thru launch, scheduled October 14. One or two 2-hour training sessions to follow.

##### Sept 16
It's been a huge leap since the last update. The stage site has a lot of content under Lab Support and some content in other places too as we test for functionality gaps.

Things that have been added:

- Search terms are scraped by the theme and given "weights" based on how often they've been searched for.
- Search results pages have different templates for the different post types.
- Training Quiz forms now send out results to a default email (in Theme Settings) or a specific email (on training edit page).
- Parent/child page relationships work better wrt the sidebar and breadcrumbs templates.

At this point, most of the heavy dev lifting is finished. Contact forms need to be hooked up to the form script, some responsive work still needs to be done, etc. Most of the work from here on out is content related.

##### Aug 28
This is the first big commit for the project! I've added all the theme image files and the rest of the template files are at probably 80 or 90% completion.

There are a bunch of different post types but for the most part the templates are pretty flexible. The only custom page templates are for the Home page (obvious) and the Resources page, since the loop on that page has some specific needs like checking for GET vars and a different loop style.

Most of the updates moving forward should be small. I wanted to wait until this close to the first stage site date before updating the repo because I was working out content types and organization.

Before arriving at this version, I had built every page in its own custom page template and had incredibly messy templates everywhere. Doing it this way cleaned it up a lot and made it easier to manage templates for all the different CPTs without having 5 different Page templates to loop them in.

Pipeline: Start adding content to staging site until functionality breaks. Hook in filters for Search, Resources, Lab Audits. Add fields/taxonomies to Resources CPT.

##### Aug 21
Prototypes are finished, EHS Managers are on board. Just beginning WordPress/dev phase. Fleshing out custom post types. We need a stage site Readme update by September 1. Let's see if I remember to do it.

##### July 9
Home page mockup and Lab Support mockup are prototyped. Grid system established; most parts should be largely reusable. By the third page prototype, I expect little to no CSS changes. Everything is tiny Sass files.

Haven't used Bourbon much yet; maybe drop Bourbon in production?