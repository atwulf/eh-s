<?php // Scrape search terms
if ( isset( $_GET['s']) ) {
    // scrape
    $search = get_option("ehs_search_terms");
    $term = $_GET['s'];

    if ( isset($search[$term]) || $search[$term] > 0 ) {
        // Add weight
        $search[$term] = $search[$term] + 1;
    } else {
        // Add search term
        $search[$term] = 1;
    }

    update_option("ehs_search_terms", $search);
} ?>

<!-- <pre><?php //var_dump($search); ?></pre> -->