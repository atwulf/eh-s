<?php get_header(); ?>

<div class="content wrapper">

    <?php get_template_part('breadcrumbs'); ?>

    <h1>Search results for: <?php echo get_search_query(); ?></h1>

    <div class="grid">

        <?php get_sidebar(); ?>

        <div class="main col-2-3">

            <?php if ( have_posts() ) {
                echo '<ul class="home-loop">';

                while ( have_posts() ) {

                    the_post();
                    get_template_part('entry');

                }

                get_template_part('nav','below');

                echo '</ul>';
            } else { ?>

                <p>Sorry, no results were found for your search.</p>

            <?php } ?>

        </div>

    </div>

</div>

<?php get_footer(); ?>