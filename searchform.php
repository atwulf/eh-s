<form class="search-form" role="search" method="get" action="<?php echo esc_url( home_url('/') ); ?>">

    <input type="text" class="search-input" name="s" placeholder="Search this site...">
    <button class="search-submit"></button>

</form>