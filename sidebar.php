<div class="sidebar col-1-4">

<?php if ( isset($_GET['s']) ) { // Special search filters

    get_template_part('templates/sidebar/search');

} elseif ( is_page('Resources') &&
           get_page_by_title('Help')->ID !== $post->post_parent ) { // Resources list filters

    get_template_part('templates/sidebar/resources');

} else { ?>

<?php // First, get the top module, usually Subpages ?>

    <?php get_template_part('templates/sidebar/top'); ?>

<?php // Get second module, usually "More Information" ?>

    <?php get_template_part('templates/sidebar/bottom'); ?>

<?php } ?>

</div>