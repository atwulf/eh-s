<li class="page-loop-post">
                            
    <h3><?php the_title(); ?></h3>

    <div class="grid">
        <div class="col-1-4">
            <strong>When</strong>
        </div>
        <div class="col-3-4">
            <p><?php the_field('audit_when'); ?></p>
        </div>
    </div>
    <div class="grid">
        <div class="col-1-4">
            <strong>Proctored by</strong>
        </div>
        <div class="col-3-4">
            <p><?php $proctor = get_field('proctored_by');
            $page = get_page_by_title( $proctor );
            // var_dump($page);
            echo $proctor;

            // if ( isset($page->ID) ) {
            //     echo '<a href="'. get_the_permalink( $page->ID ) .'">'. $proctor .'</a>';
            // } else {
            //     echo $proctor;
            // } ?></p>
        </div>
    </div>
    <div class="grid">
        <div class="col-1-4">
            <strong>About</strong>
        </div>
        <div class="col-3-4">
            <p><?php the_field('audit_about'); ?></p>
        </div>
    </div>
    <?php if ( get_field('related_resource') ) { ?>
    <div class="grid">
        <div class="col-1-4">
            <strong>Related resource</strong>
        </div>
        <div class="col-3-4">
            <p><?php $resource = get_field('related_resource');
            echo '<a href="'. get_the_permalink($resource->ID) .'">'. $resource->post_title .'</a>'; ?></p>
        </div>
    </div>
    <?php } ?>

</li>