<?php
// Get top parent
$parents = ehs_get_page_parents( get_the_ID() );
$top_page = get_page( $parents['top'] );

// Get loop and category
$loop = get_field('cpt_loop');
if ( get_field('cpt_category') ) $cat = get_cat_name( get_field('cpt_category') );

// Init WP_Query args
$args = array(
    'post_type' => $loop
);

// Set order and orderby
$order = 'DESC';
$orderby = 'date';
if ( $loop == 'trainings' || $loop == 'audits' ) {
    $order = 'ASC';
    $orderby = 'title';
} else if ( $loop == 'staff' ) {
    $order = 'ASC';
    $orderby = 'menu_order';
}
$args['order'] = $order;
$args ['orderby'] = $orderby;

// Set category
if ( isset($cat) ) $args['departments'] = $cat;

// Set posts per page
$posts_per_page = -1;
if ( 'About' == $top_page->post_title ) $posts_per_page = 2;
$args['posts_per_page'] = $posts_per_page;

// GET filters for trainings
if ( 'trainings' == $loop ) {
    $for = 'null';
    $with = 'null';
    if ( isset($_GET['f']) ) $for = $_GET['f'];
    if ( isset($_GET['w']) ) $with = $_GET['w'];

    if ( $for !== 'null' ) $args['intended_for'] = $for;
    if ( $with !== 'null' ) $args['research_with'] = $with;
}

// Special loop style for Staff listings
if ( $loop == 'staff' ) { # loop units alphabetically
    $units = get_terms('departments');
    foreach ( $units as $unit ) {
        $args['departments'] = $unit->slug;

        $staff_loop = new WP_Query($args);

        if ( $staff_loop->have_posts() ) {
            echo '<a name="' . $unit->slug .'"></a>';
            echo '<h3 id="#'. $unit->slug .'">'. $unit->name .'</h3>';
            echo '<ul class="'. ( 'post' == $loop ? 'home-loop' : 'page-loop' ) .'">';
            echo '<div class="grid">';

            while ( $staff_loop->have_posts() ) {
                $staff_loop->the_post();

                get_template_part('entry');
            }

            echo '</div></ul>';
        }
    }

} else { # default loop by name alphabetically
    $cpt_loop = new WP_Query($args);
    if ( $cpt_loop->have_posts() ) {
        echo '<ul class="'. ( 'post' == $loop ? 'home-loop' : 'page-loop' ) .'">';
        if ( 'staff' == $loop ) echo '<div class="grid">';

        while ( $cpt_loop->have_posts() ) {
            $cpt_loop->the_post();

            get_template_part('entry');
        }

        if ( 'staff' == $loop ) echo '</div>';
        echo '</ul>';
    } else {
        echo '<p>Sorry, nothing was found for your search.</p>';
    }
}

wp_reset_postdata(); ?>