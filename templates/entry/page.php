<?php
$parents = ehs_get_page_parents( get_the_ID() );
$top_page = get_page($parents['top']); ?>

<h1><?php echo $top_page->post_title; ?></h1>

<div class="grid">
    
    <?php get_sidebar(); ?>

    <div class="main col-2-3">

        <?php if ( sizeof( $parents['parents'] ) > 0 ) {

            echo '<h2>'. get_the_title() .'</h2>';

        } ?>

        <?php the_content(); ?>

        <?php // Get relevant contact forms
        if ( 'Report a Campus Laboratory Concern' == get_the_title() || 1761 == get_the_ID() )
            $form = 'lab_concern';
        if ( 'Radioactive Waste Pickup' == get_the_title() || 2246 == get_the_ID() )
            $form = 'rad_waste';
        if ( 'Contact' == get_the_title() )
            $form = 'contact';

        if ( isset($form) )
            get_template_part('templates/forms/'. $form); ?>

        <?php if ( 'Training' == $top_page->post_title && 'Training' == get_the_title() ) { ?>
            
            <?php get_template_part('training', 'filters'); ?>

        <?php } ?>

        <?php

        if ( get_field('cpt_loop') ) {

            // About pages loop prefix
            if ( $top_page->post_title == 'About' ) {
                echo '<div class="grid"><div class="col-3-4"><div class="module"><h4>News and Updates</h4>';
            }

            get_template_part('templates/entry/page', 'loop');

            // About pages loop postfix
            if ( $top_page->post_title == 'About') echo '</div></div></div>';

        } ?>

    </div>

</div>