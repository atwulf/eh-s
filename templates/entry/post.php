<?php if ( is_single() ) { ?>

    <h1>News and Updates</h1>

    <div class="grid">
        
        <?php get_sidebar(); ?>

        <div class="main col-2-3">

            <h2><?php the_title(); ?></h2>

            <p class="post-date">Posted on <?php echo get_the_date('F j, Y');
                if ( get_the_author() ) echo ' by '. get_the_author();

                // get post terms
                $units = wp_get_post_terms( get_the_ID(), 'departments' );
                $terms = array();

                if ( sizeof($units) > 0 ) {
                    foreach ( $units as $unit ) {
                        $terms[] = $unit->name;
                    }
                    $terms = implode( ', ', $terms );

                    echo ' in '. $terms;
                } ?></p>

            <?php the_content(); ?>

        </div>

    </div>

<?php } else { ?>

    <li class="home-loop-post">
                                    
        <span class="date"><?php echo get_the_date('F j, Y'); ?></span>
        <a href="<?php the_permalink(); ?>" class="post-title"><?php the_title(); ?></a>
        <?php the_excerpt(); ?>
        <a href="<?php the_permalink(); ?>" class="read-more">Read more...</a>

    </li>

<?php } ?>