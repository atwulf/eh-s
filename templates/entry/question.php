<li class="question-single">
    
    <div class="prompt">
        <p><?php the_title(); ?></p>
    </div>

    <?php if ( get_field('question_image') ) {
        echo '<img src="'. get_field('question_image') .'" alt="" />';
    } ?>

    <?php
    $question = get_the_title();
    $type = get_field('question_type');
    $answers = get_field('quiz_answers');
    $answers = explode("\n", $answers);
    $count = get_the_ID();

    if ( get_field('question_type') == 'textarea' ) { ?>

        <label for="<?php echo 'answer-'. $count; ?>">

            <span>Enter your answer.</span>
            <textarea name="<?php echo 'answer-'. $count; ?>" id="<?php echo 'answer-'. $count; ?>" class="quiz-textarea"></textarea>

        </label>

    <?php } else {

        echo '<ol class="responses">';
        foreach ( $answers as $answer ) { ?>

            <?php $val = trim($answer);
                  $id = strip_tags(str_replace(' ', '-', $val)); ?>
            <li>
                <label for="<?php echo $id .'-'. $count; ?>">

                    <input type="radio" id="<?php echo $id . '-' . $count; ?>" name="<?php echo 'answer-'. $count; ?>" value="<?php echo $val; ?>"><span><?php echo $val; ?></span>


                </label>
            </li>

        <?php }
        echo '</ol>'; ?>

    <?php } ?>

    <input type="hidden" name="<?php echo 'question-'. $count; ?>" value="<?php the_title(); ?>">

</li>