<?php if ( is_single() ) { 
    // Single Resource Page ?>
<h1>Resources</h1>

<div class="grid">
    
    <?php get_sidebar(); ?>

    <div class="main col-2-3">

        <h2><?php the_title(); ?></h2>

        <?php $resource_type = get_field('resource_type');
        if ( $resource_type == 'page' ) : ?>

            <?php if ( get_field('intended_for') ) {
                echo '<p>This Environmental Health & Safety guideline documentation is intended for '. get_field('intended_for') .'.</p>';
            } ?>

            <?php echo '<p>Revised as of '. get_the_date('F j, Y'); ?>

            <?php if ( get_field('related_link') ) {
                echo 'See also: ';
            } ?>

        <?php endif; ?>

        <?php if ( get_field('short_description') ) { ?>
            <h3>General Description</h3>
            <?php the_field('short_description'); ?>
        <?php } ?>

        <article class="resource-main">
            
            <?php
            if ( $resource_type == 'page' ) {

                the_content();

            } elseif ( $resource_type == 'link' ) { ?>

                <?php the_content(); ?>
                <a href="<?php the_field('resource_url'); ?>" class="button-gold" target="_blank">View Resource</a>

            <?php
            } elseif ( $resource_type == 'file' ) { ?>

                <a href="<?php the_field('resource_file'); ?>" class="button-gold" target="_blank">Download Resource</a>
                <?php
                $file = get_field('resource_file');
                $extension = strtoupper( end( explode('.', $file) ) );
                echo '<p class="file-type">File type: '. $extension .' | Size: '. file_size($file) .'</p>';

            } ?>

        </article>

    </div>

</div>

<?php } else { // page-resources.php loop ?>

<div class="grid resource-single">
    
    <div class="col-1-2">
        <a href="<?php the_permalink(); ?>"><?php the_title(); ?><?php 
            if ( get_field('resource_type') == 'file' ) {
                $file = get_field('resource_file');
                $extension = strtoupper( end( explode('.', $file) ) );
                echo ' ['. $extension .']';
            }
        ?></a>
    </div>
    <div class="col-1-4">
        <?php echo get_the_date('F j, Y'); ?>
    </div>
    <div class="col-1-4">
        <?php $departments = wp_get_post_terms( get_the_ID(), 'departments' );
        end($departments);
        $last_key = key($departments);
        foreach ( $departments as $key => $dept ) {
            echo $dept->name;
            if ( $key !== $last_key ) echo ', ';
        }
        ?>
    </div>

</div>

<?php } ?>