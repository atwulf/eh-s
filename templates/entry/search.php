<?php
$parents = ehs_get_page_parents();
if ( sizeof($parents['parents']) > 0 ) $top = get_the_title($parents['top']);

$post_type = get_post_type();
if ( 'trainings' == $post_type ) $top = 'Training';
if ( 'resources' == $post_type ) $top = 'Resources';
if ( 'audits' == $post_type ) $top = 'Lab Audits';

?>

<li class="sr-<?php echo $post_type; ?>">

    <?php if ( isset($top) ) : ?>
    <span class="search-parent">
        <?php echo $top; ?>
    </span>
    <?php endif; ?>

    <h4><a href="<?php if ( $post_type == 'audits' ) {
        // Single Lab Audits results point to the Lab Audits loop page.
        echo get_the_permalink( get_page_by_title('Lab Audits & Inspections') );
    } else {
        the_permalink();
    } ?>">
        <?php the_title(); ?>
    </a></h4>

    <?php // POST TYPE BODIES
    if ( 'trainings' == $post_type ) : // Trainings ?>

        <div class="grid">
            <div class="col-1-4">
                <strong>Intended for</strong>
            </div>
            <div class="col-3-4">
                <p>
                <?php $intended_for = wp_get_post_terms( get_the_ID(), 'intended_for' );
                end($intended_for);
                $last_key = key($intended_for);
                foreach ( $intended_for as $key => $for ) {
                    echo ucfirst($for->name);
                    if ( $key !== $last_key ) echo ', ';
                }
                ?></p>
            </div>
        </div>

        <div class="grid">
            <div class="col-1-4">
                <strong>Required frequency</strong>
            </div>
            <div class="col-3-4">
                <p><?php the_field('training_when'); ?></p>
            </div>
        </div>

        <div class="grid">
            <div class="col-1-4">
                <strong>Where</strong>
            </div>
            <div class="col-3-4">
                <p><?php
                    if ( get_field('offsite_quiz') == 'In Person' ) {
                        echo '1000 Regent Dr.';
                    } else {
                        echo 'Online';
                    }
                ?></p>
            </div>
        </div>

        <?php
        echo '<div class="related-pages">Jump to: ';
        echo '<a href="'. get_permalink() .'"">Overview</a>, '; 
        $subpages = get_field('offsite_quiz');
        if ( get_field('course_material') !== '' ) {
            echo '<a href="'. get_permalink() .'?page=course_material">Course Material</a>, '; 
        }
        if ( $subpages == 'On This Website' ) {
            echo '<a href="'. get_permalink() .'?page=quiz">Completion Quiz</a>'; 
        } elseif ( $subpages == 'In Person' ) {
            echo '<a href="'. get_permalink() .'?page=signup">Course Signup</a>'; 
        }
        echo '</div>'; ?>

    <?php elseif ( 'resources' == $post_type ) : // Resources ?>

        <?php the_field('short_description'); ?>

    <?php elseif ( 'audits' == $post_type ) : // Lab Audits ?>

        <div class="grid">
            <div class="col-1-4">
                <strong>When</strong>
            </div>
            <div class="col-3-4">
                <p><?php the_field('audit_when'); ?></p>
            </div>
        </div>
        <div class="grid">
            <div class="col-1-4">
                <strong>Proctored by</strong>
            </div>
            <div class="col-3-4">
                <p><?php $proctor = get_field('proctored_by'); ?>
                <?php echo $proctor->post_title; ?></p>
            </div>
        </div>
        <div class="grid">
            <div class="col-1-4">
                <strong>About</strong>
            </div>
            <div class="col-3-4">
                <p><?php the_field('audit_about'); ?></p>
            </div>
        </div>

    <?php else : // Page ?>

        <?php $excerpt = get_the_excerpt();
        $excerpt = explode('</p>...', $excerpt);
        echo $excerpt[0] . '...</p>'; ?>

    <?php endif; ?>

    <?php // POST TYPE FOOTERS
    if ( isset($top) && get_post_type() == 'page' ) { // Child pages show sibling pages
        $siblings = get_pages(array(
            'parent' => $parents['top'],
            'exclude' => $post->ID,
            'hierarchical' => 0
        ));

        echo '<div class="related-pages">Related pages: ';
        for ($i=0; $i < 3; $i++) {
            echo '<a href="'. get_the_permalink($siblings[$i]->ID) .'">'. $siblings[$i]->post_title .'</a>';
            if ($i < 2) echo ', ';
        }
        echo '</div>';

    } elseif ( $top == 'Resources' ) { // Resources shows first headings

        // Filter the content for the first three h1 tags
        $content = get_the_content();

        $content = str_ireplace( '<h1', '%%H1%%', $content );
        $content = str_ireplace( '/h1>', '%%H1%%', $content );
        $content = explode('%%H1%%', $content);

        foreach ( $content as $text ) {
            if ( $text[0] == '>' ) {
                $text = explode('>', $text);
                $text = explode('<', $text[1]);
                $headings[] = $text[0];
            }
        }

        $count = 0;

        foreach ( $headings as $h ) {
            $slug = str_ireplace( ' ', '', $h );
            $slug = str_ireplace( '.', '', $slug );
            $slug = str_ireplace('(', '', $slug);
            $slug = str_ireplace(')', '', $slug);
            $slug = str_ireplace('&amp;', '', $slug);
            $slug = strtolower($slug);

            if ( $slug !== '' && $count < 3 ) {
                $links[] = array(
                    'text' => $h,
                    'slug' => $slug
                );
            }

            $count = $count + 1;
        }

        if ( count($links) > 0 ) {
            array_slice($links, 0, 3);

            end($links);
            $last_key = key($links);

            echo '<div class="related-pages">Jump to: ';
            foreach ($links as $key => $head) {
                echo '<a href="'. get_the_permalink() .'#'. $head['slug'] .'">'. $head['text'] .'</a>';
                if ($key !== $last_key) echo ', ';
            }
            echo '</div>';
        }

    } else { // Default to showing child pages
        $children = get_pages(array(
            'parent' => $post->ID,
            'exclude' => $post->ID,
            'hierarchical' => 0
        ));

        if ( count($children) > 0 ) {
            echo '<div class="related-pages">Related pages: ';
            for ($i=0; $i < 3; $i++) {
                echo '<a href="'. get_the_permalink($children[$i]->ID) .'">'. $children[$i]->post_title .'</a>';
                if ($i < 2) echo ', ';
            }
            echo '</div>';
        }
    } ?>

</li>