<div class="col-1-2">
<li class="page-loop-post staff-loop">

    <h4><?php
        $name = get_the_title();

        if ( strpos($name, ', ') > 0 ) {
            $name = explode(', ', $name);
            echo $name[1] .' '. $name[0];
        } else {
            echo $name;
        }

    ?></h4>
<?php if ( isset( $_GET['v'] ) ) : ?>
    <div class="grid">
        <div class="col-1-4">
            <strong>Unit</strong>
        </div>
        <div class="col-3-4">
            <?php $departments = wp_get_post_terms( get_the_ID(), 'departments' );
            end($departments);
            $last_key = key($departments);
            foreach ( $departments as $key => $dept ) {
                echo $dept->name;
                if ( $key !== $last_key ) echo ', ';
            }
            ?>
        </div>
    </div>
<?php endif; ?>

<?php if ( get_field('staff_role') ) : ?>
    <div class="grid">
        <div class="col-1-4">
            <strong>Role</strong>
        </div>
        <div class="col-3-4">
            <?php the_field('staff_role'); ?>
        </div>
    </div>
<?php endif; ?>

<?php if ( get_field('staff_email') ) : ?>
    <?php $email = get_field('staff_email'); ?>
    <div class="grid">
        <div class="col-1-4">
            <strong>Email</strong>
        </div>
        <div class="col-3-4">
            <a href="mailto:<?php echo antispambot($email); ?>"><?php echo antispambot($email); ?></a>
        </div>
    </div>
<?php endif; ?>

<?php if ( get_field('staff_phone') ) : ?>
    <div class="grid">
        <div class="col-1-4">
            <strong>Phone</strong>
        </div>
        <div class="col-3-4">
            <?php the_field('staff_phone'); ?>
        </div>
    </div>
<?php endif; ?>
    
</li>
</div>