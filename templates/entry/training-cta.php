<?php
/**
 * Template partial to correctly set the Call to Action button
 * on Training Overviews and in Training Loop
 */

$form_type = get_field('training_form_type');
$button = 'Learn More';

if ($form_type == 'quiz') $button = 'Take Quiz';
if (get_field('course_material')) $button = 'Take Course';
if ($form_type == 'info') $button = 'Learn More';
if ($form_type == 'schedule') $button = 'Sign Up';
if ($form_type == 'offsite') $button = 'Sign Up';
if ($form_type == 'offsite' && get_field('course_material')) $button = 'Take Quiz';

$permalink = get_the_permalink(get_the_ID());
$href = $permalink;
if ($form_type !== 'hide') $href = $permalink .'?page=form';
if (get_field('course_material') && is_single()) $href = $permalink .'?page=course_material';
if (get_field('course_material') && !is_single()) $href = $permalink;
if ($form_type == 'offsite') $href = get_field('offsite_url');
if ($form_type == 'offsite' && !is_single()) $href = $permalink; ?>

<?php if ( $form_type !== 'hide' ) {
    echo '<a href="'. $href .'"';
    if ($form_type == 'offsite' && is_single()) echo ' target="_blank"';
    echo ' class="button-gold">'. $button .'</a>';
} else {
    if ( ! is_single() ) echo '<a href="'. $href .'" class="button-gold">'. $button .'</a>';
}

if ( $form_type == 'quiz' &&
     get_field('course_material') &&
     is_single() ) {
    echo '<a href="'. $permalink .'?page=form"';
    echo ' class="button-gold">Take Quiz</a>';
} ?>