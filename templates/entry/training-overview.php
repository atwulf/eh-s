<?php
// Set up Upcoming Sessions array
if ( get_field('upcoming_sessions') )
    $upcoming_sessions = explode("\n", trim( get_field('upcoming_sessions') )); ?>

<ul class="page-loop">
    <li class="page-loop-post">

    <h2><?php the_title(); ?></h2>

    <div class="grid">
        <div class="col-1-4">
            <strong>Intended for</strong>
        </div>
        <div class="col-3-4">
            <p><?php $for = get_the_terms( get_the_ID(), 'intended_for' );
            foreach ($for as $for) {
                // var_dump($for);
                $term_names[] = $for->name;
            }
            echo implode(', ', $term_names); ?></p>
        </div>
    </div>

    <div class="grid">
        <div class="col-1-4">
            <strong>Proctored by</strong>
        </div>
        <div class="col-3-4">
            <?php $proctor = get_field('proctored_by'); ?>
            <?php echo '<p><a href="'. get_the_permalink($proctor->ID) .'">'. $proctor->post_title .'</a></p>'; ?>
        </div>
    </div>

    <div class="grid">
        <div class="col-1-4">
            <strong>Required frequency</strong>
        </div>
        <div class="col-3-4">
            <p><?php the_field('training_when'); ?></p>
        </div>
    </div>

    <div class="grid">
        <div class="col-1-4">
            <strong>Where</strong>
        </div>
        <div class="col-3-4">
            <p><?php $where = get_field('training_where');
            if ( !empty($where) ) {

                the_field('training_where');

            } else {
                if ( get_field('offsite_quiz') == 'In Person' ) {
                    echo '1000 Regent Dr.';
                } else {
                    echo 'Online';
                }
            }
            ?></p>
        </div>
    </div>
<?php $sessions = get_field('upcoming_sessions');
if ( $sessions ) : ?>
    <div class="grid">
        <div class="col-1-4">
            <strong>Upcoming sessions</strong>
        </div>
        <div class="col-3-4">
            <p><select name="upcoming-sessions">
                <?php foreach ($upcoming_sessions as $ucs) {
                    echo '<option>'. $ucs .'</option>';
                } ?>
            </select></p>
        </div>
    </div>
<?php endif; ?>

    </li>
</ul>

<h3><?php the_title(); ?> Overview</h3>
<?php the_field('training_overview'); ?>

<?php get_template_part('templates/entry/training', 'cta');