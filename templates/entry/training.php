<?php
// Set up Upcoming Sessions array
if ( get_field('upcoming_sessions') ) {
    $sessions = get_field('upcoming_sessions');
    $upcoming_sessions = explode("\n", trim( $sessions ));
}

if ( is_single() ) { // Custom Post Type with subpages ?>

<h1>Training</h1>

<div class="grid">

    <?php get_sidebar(); ?>

    <div class="main col-2-3">

        <?php $curr_page = $_GET['page'];

        // Display the correct page
        if ( $curr_page == 'course_material' && get_field('course_material') ) {

            echo '<h2>'. get_the_title() .' Course Material</h2>';
            the_field('course_material');

            if ( get_field('training_form_type') == 'quiz' ) {
                $url = get_the_permalink() . '?page=form';
                echo '<a class="button-gold" href="'. $url .'">Take Quiz</a>';
            }

        } elseif ( $curr_page == 'form' && get_field('training_form_type') !== 'hide' ) {

            get_template_part('templates/forms/training');

        } else {

            get_template_part('templates/entry/training', 'overview');

        } ?>

    </div>

</div>

<?php } else { // Loop of trainings on the master Training page ?>

<?php if ( $_GET['v'] == 'list' || !isset($_GET['v']) ) { ?>

    <li class="page-loop-post">

        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

        <div class="grid">
            <div class="col-1-4">
                <strong>Intended for</strong>
            </div>
            <div class="col-3-4">
                <p><?php $for = get_the_terms( get_the_ID(), 'intended_for' );
                foreach ($for as $for) {
                    $term_names[] = ucfirst($for->name);
                }
                echo implode(', ', $term_names); ?></p>
            </div>
        </div>

        <div class="grid">
            <div class="col-1-4">
                <strong>Required frequency</strong>
            </div>
            <div class="col-3-4">
                <p><?php the_field('training_when'); ?></p>
            </div>
        </div>

        <?php get_template_part('templates/entry/training', 'cta'); ?>

    </li>

<?php } else { ?>

<li class="page-loop-post">

    <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

    <div class="grid">
        <div class="col-1-4">
            <strong>Intended for</strong>
        </div>
        <div class="col-3-4">
            <p><?php $for = get_the_terms( get_the_ID(), 'intended_for' );
            foreach ($for as $for) {
                $term_names[] = ucfirst($for->name);
            }
            echo implode(', ', $term_names); ?></p>
        </div>
    </div>

    <div class="grid">
        <div class="col-1-4">
            <strong>Proctored by</strong>
        </div>
        <div class="col-3-4">
            <?php $proctor = get_field('proctored_by'); ?>
            <?php echo '<p><a href="'. get_the_permalink($proctor->ID) .'">'. $proctor->post_title .'</a></p>'; ?>
        </div>
    </div>

    <div class="grid">
        <div class="col-1-4">
            <strong>Required frequency</strong>
        </div>
        <div class="col-3-4">
            <p><?php the_field('training_when'); ?></p>
        </div>
    </div>

    <div class="grid">
        <div class="col-1-4">
            <strong>Where</strong>
        </div>
        <div class="col-3-4">
            <p><?php $where = get_field('training_where');
            if ( !empty($where) ) {

                the_field('training_where');

            } else {
                if ( get_field('offsite_quiz') == 'In Person' ) {
                    echo '1000 Regent Dr.';
                } else {
                    echo 'Online';
                }
            }
            ?></p>
        </div>
    </div>

<?php if ( get_field('upcoming_sessions') ) : ?>
    <div class="grid">
        <div class="col-1-4">
            <strong>Upcoming sessions</strong>
        </div>
        <div class="col-3-4">
            <p><select name="upcoming-sessions">
                <?php foreach ($upcoming_sessions as $ucs) {
                    echo '<option>' . $ucs . '</option>';
                } ?>
            </select></p>
        </div>
    </div>
<?php endif; ?>

    <div class="grid">
        <div class="col-1-4">
            <strong>More information</strong>
        </div>
        <div class="col-3-4">
            <?php the_field('training_overview'); ?>
        </div>
    </div>

    <?php get_template_part('templates/entry/training', 'cta'); ?>

</li>
<?php
}

} ?>