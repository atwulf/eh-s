<?php
/**
 * Create a dropdown list of building codes and names
 * to be used on any contact form that requires it.
 */

$codes = get_option("ehs_building_codes");

echo '<select id="building-code" name="building-code">';
echo '<option value="N/A">Select a Building</option>';

// Now loop through them and create a select box
foreach ($codes as $code => $bldg) {
    echo '<option value="'. $code .'">'. $code .' - '. $bldg .'</option>';
}

// Close the select and hope for the best?
echo '</select>';