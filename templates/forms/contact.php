<?php if ( isset($_GET['complete']) ) { ?>

    <h3>Thank you!</h3>
    <p>We appreciate your concern. An Environmental Health &amp; Safety representative will contact you about your request as soon as possible.</p>

<?php } else { ?>

<h2>Contact <?php
    $units = array(
        'asb' => 'Asbestos & Lead Management',
        'bio' => 'Biosafety',
        'ecih' => 'Environmental Compliance & Industrial Hygiene',
        'haz' => 'Hazardous Materials & Waste Management',
        'rad' => 'Radiation Safety'
    );
    if ( isset($_GET['unit']) ) {
        $unit = $_GET['unit'];
        echo $units[$unit];
    } else {
        echo 'Environmental Health & Safety';
    }
?></h2>

<p>Have a question for us? Feel free to drop us a line and we’ll get back to you as soon as possible.</p>

<form action="<?php bloginfo('template_url'); ?>/forms.php?q=lab+concern" method="post" class="quiz">

    <div class="grid">
        <div class="col-1-2">
            <label for="first-name">
                <span>Your First Name</span>
                <input type="text" id="first-name" name="first-name">
            </label>
        </div>
        <div class="col-1-2">
            <label for="last-name">
                <span>Your Last Name</span>
                <input type="text" id="last-name" name="last-name">
            </label>
        </div>
    </div>

    <div class="grid">
        <div class="col-1-2">
            <label for="email-address">
                <span>Your Email</span>
                <input type="text" id="email-address" name="email-address">
            </label>
        </div>
        <div class="col-1-2">
            <label for="phone-number">
                <span>Contact Phone Number</span>
                <input type="text" id="phone-number" name="phone-number">
            </label>
        </div>
    </div>

    <div class="grid">
        <label for="message">
            <span>Your Message</span>
            <textarea name="message" id="message"></textarea>
        </label>
    </div>

    <?php get_template_part('templates/forms/recipient'); ?>
    <input type="hidden" name="q" value="contact">
    <input type="hidden" name="unit" value="<?php echo $_GET['unit']; ?>">

    <button class="button-gold">Send Message</button>
    
</form>

<?php } ?>