<?php if ( isset($_GET['complete']) ) { ?>

    <h3>Thank you!</h3>
    <p>We appreciate your concern. An Environmental Health &amp; Safety representative will contact you about your request as soon as possible.</p>

<?php } else { ?>

<h5>All fields are required.</h5>

<form action="<?php bloginfo('template_url'); ?>/forms.php?q=lab+concern" method="post" class="quiz">

    <div class="grid">
        <div class="col-1-2">
            <label for="first-name">
                <span>Your First Name</span>
                <input type="text" id="first-name" name="first-name">
            </label>
        </div>
        <div class="col-1-2">
            <label for="last-name">
                <span>Your Last Name</span>
                <input type="text" id="last-name" name="last-name">
            </label>
        </div>
    </div>

    <div class="grid">
        <div class="col-1-2">
            <label for="email-address">
                <span>Your Email</span>
                <input type="text" id="email-address" name="email-address">
            </label>
        </div>
        <div class="col-1-2">
            <label for="phone-number">
                <span>Contact Phone Number</span>
                <input type="text" id="phone-number" name="phone-number">
            </label>
        </div>
    </div>

    <div class="grid">
        <div class="col-1-2">
            <label for="supervisor">
                <span>Supervisor/PI</span>
                <input type="text" id="supervisor" name="supervisor">
            </label>
        </div>
        <div class="col-1-2 grid">
            <label for="building-code" class="col-1-2">
                <span>Building Code</span>
                <?php get_template_part('templates/forms/building'); ?>
            </label>
            <label for="room-number" class="col-1-2">
                <span>Room Number</span>
                <input type="text" id="room-number" name="room-number">
            </label>
        </div>
    </div>

    <div class="grid">
        <div class="col-1-2">
            <label for="role">
                <span>Role</span>
                <select name="role" id="role">
                    <option value="Principal Investigator">Principal Investigator</option>
                    <option value="Lab Proctor">Lab Proctor</option>
                    <option value="Generator">Generator</option>
                    <option value="Faculty/Staff">Faculty/Staff</option>
                    <option value="Undergrad">Student - Undergrad</option>
                    <option value="Graduate Student">Student - Graduate</option>
                    <option value="PostDoc">Postdoctoral</option>
                    <option value="Other">Other</option>
                </select>
            </label>
            <label for="nature-of-concern">
                <span>Nature of Concern</span>
                <select name="nature-of-concern" id="nature-of-concern">
                    <option value="Asbestos/Lead">Asbestos/Lead</option>
                    <option value="Biohazard">Biohazard</option>
                    <option value="Hazardous Waste">Hazardous Waste</option>
                    <option value="Radiation">Radiation</option>
                    <option value="Environmental">Environmental</option>
                    <option value="Other">Other</option>
                </select>
            </label>
        </div>
        <div class="col-1-2">
            <label for="concern-details">
                <span>Details</span>
                <textarea id="concern-details" name="concern-details"></textarea>
            </label>
        </div>
    </div>

    <?php get_template_part('templates/forms/recipient'); ?>
    <input type="hidden" name="q" value="safety">

    <button class="button-gold">Submit Report</button>
    
</form>

<?php } ?>