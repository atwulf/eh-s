<?php if ( isset($_GET['complete']) ) { ?>

    <h3 id="page-title">Thank you!</h3>
    <p>We appreciate your concern. A Radiation Safety representative will contact you about your request as soon as possible.</p>

<?php } else { ?>

<form action="<?php bloginfo('template_url'); ?>/forms.php" method="post" class="quiz">

    <div class="grid">
        <div class="col-1-2">
            <label for="first-name">
                <span>Your First Name</span>
                <input type="text" id="first-name" name="first-name">
            </label>
        </div>
        <div class="col-1-2">
            <label for="last-name">
                <span>Your Last Name</span>
                <input type="text" id="last-name" name="last-name">
            </label>
        </div>
    </div>

    <div class="grid">
        <div class="col-1-2">
            <label for="email-address">
                <span>Your Email</span>
                <input type="text" id="email-address" name="email-address">
            </label>
        </div>
        <div class="col-1-2">
            <label for="phone-number">
                <span>Contact Phone Number</span>
                <input type="text" id="phone-number" name="phone-number">
            </label>
        </div>
    </div>

    <div class="grid">
        <div class="col-1-2">
            <label for="supervisor">
                <span>Supervisor/PI</span>
                <input type="text" id="supervisor" name="supervisor">
            </label>
        </div>
        <div class="col-1-2 grid">
            <label for="building-code" class="col-1-2">
                <span>Building Code</span>
                <?php get_template_part('templates/forms/building'); ?>
            </label>
            <label for="room-number" class="col-1-2">
                <span>Room Number</span>
                <input type="text" id="room-number" name="room-number">
            </label>
        </div>
    </div>

    <div class="grid">
        <div class="col-1-2">
            <label for="department">
                <span>Department</span>
                <input type="text" id="department" name="department">
            </label>
        </div>
        <div class="col-1-2">
            <label for="wipes">
                <span>Have wipes been taken on all containers?</span>
                <select name="wipes" id="wipes">
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
            </label>
        </div>
    </div>

    <div class="grid">
        <div class="col-3-4">
            <label for="message">
                <span>Additional Information</span>
                <textarea name="message" id="message"></textarea>
            </label>
        </div>
    </div>

    <div class="container-request">
        <h4>Container 1</h4>

        <div class="grid">
            <div class="col-1-2">
                <label for="waste-type-1">

                    <span>Type of waste</span>
                    <select name="waste-type-1" id="waste-type-1" class="container-fieldset">
                        <option value="null" disabled>Select an option...</option>
                        <option value="Solid">Solid</option>
                        <option value="Liquid">Liquid</option>
                        <option value="Scintillation vials">Scintillation Vials</option>
                        <option value="Lead pigs">Lead Pigs</option>
                        <option value="Radioactive sharps">Radioactive Sharps</option>
                    </select>

                </label>
            </div>
            <div class="col-1-2">
                <label for="waste-volume-1">

                    <span>Volume of waste</span>
                    <select name="waste-volume-1" id="waste-volume-1" class="container-fieldset">
                        <option value="null">Select an option...</option>
                        <option value="1 gallon">1 gallon</option>
                        <option value="5 gallon">5 gallons</option>
                        <option value="20 gallon">20 gallons</option>
                        <option value="Sharps">Sharps Container</option>
                        <option value="Lead Pig box">Lead Pig box</option>
                    </select>

                </label>
            </div>
        </div>

        <div class="grid">
            <label>
                <?php // This is dumb, get rid of it at some point
                    $iso_width = '200px'; ?>

                <span>Isotopes and Total Activity (mCi)</span>
                <p class="module" style="background-color:#fff;">
                    #1: <input type="text" name="isotope_1-1" size="1" style="max-width: <?php echo $iso_width; ?>;" class="container-fieldset"> at <input type="text" name="activity_1-1" size="1" style="max-width: <?php echo $iso_width; ?>;" class="container-fieldset"> mCi
                <br />
                    #2: <input type="text" name="isotope_2-1" size="1" style="max-width: <?php echo $iso_width; ?>;" class="container-fieldset"> at <input type="text" name="activity_2-1" size="1" style="max-width: <?php echo $iso_width; ?>;" class="container-fieldset"> mCi
                <br />
                    #3: <input type="text" name="isotope_3-1" size="1" style="max-width: <?php echo $iso_width; ?>;" class="container-fieldset"> at <input type="text" name="activity_3-1" size="1" style="max-width: <?php echo $iso_width; ?>;" class="container-fieldset"> mCi
                </p>

            </label>
        </div>
        <div class="grid">
            <div class="col-1-3">

                <label for="ph-1">

                    <p><span>If this is a liquid, what is the pH?</span>
                    <input type="text" name="ph-1" id="ph-1" class="container-fieldset"></p>

                </label>

            </div>
            <div class="col-1-3">
                <label for="need-replacement-1">

                    <p><span>Do you need an empty container to replace this one?</span>
                    <select name="need-replacement-1" id="need-replacement-1" class="container-fieldset">
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select></p>

                </label>
            </div>
            <div class="col-1-3">

                <label for="waste-color-1">

                    <span>Container color</span>
                    <p class="module" style="background-color:#fff;"><input type="radio" name="waste-color-1" value="Yellow" class="container-fieldset"> <span style="color: #FFCC00;">Yellow</span><br/>
                    <input type="radio" name="waste-color-1" value="Orange" class="container-fieldset"> <span style="color: #FF6600;">Orange</span><br/>
                    <input type="radio" name="waste-color-1" value="Green" class="container-fieldset"> <span style="color: #00CC00;">Green</span></p>

                </label>

            </div>
        </div>

        <div class="grid">
            <div class="col-3-4">

                <label for="constituents-1" class="grid">

                    <span>Constituents</span>
                    <textarea name="constituents-1" id="constituents-1" class="container-fieldset taller-textarea"></textarea><p/>
                    <p style="font-size: .8em;">Enter the items and % by volume in your waste. The percentages must equal 100%. Example for a liquid: "Tris 50%, water 25%, ethanol 25%"</p>

                </label>

            </div>
        </div>
    </div>

    <p><span class="add-container button-secondary">Add Another Container</span></p>

    <p class="containers-total module">Total number of containers in this request: <span id="container-count">0</span> / 5</p>

    <?php get_template_part('templates/forms/recipient'); ?>
    <input type="hidden" name="q" value="rad_waste">
    <input type="hidden" name="container-types-count" value="1">
    <input type="hidden" name="date_entered" value="<?php echo date('Y-m-d'); ?>">

    <button class="button-gold">Submit Request</button>

</form>

<?php } ?>