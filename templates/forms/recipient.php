<?php
/**
 * Deliver the correct email address for the form recipient.
 * Now with multiple possible values!
 *
 * Defaults to the theme settings backup email when applicable.
 * Otherwise, set by individual post custom fields.
 * get_field('form_email') == (string) Hand-typed email address
 * get_field('form_staff') == (object) Staff post type post object
 *
 * Failsafe email is the developer's, as an absolute last resort.
 */

$all_emails = antispambot('austin@thatbrightrobot.com');
$email_address = get_field('form_email'); // CSV hand-entered emails
$email_staff = get_field('form_staff'); // Multiple or single staff members
$email_default = get_option('ehs_default_email'); // Default email in theme settings

// First check for any hand-entered emails
$email_address = str_replace(' ', '', $email_address);
if ( strpos($email_address, ',') !== false ) {
    // string has multiple values
    $custom_emails = explode(',', $email_address);
    $custom_emails = array_filter($custom_emails);
} else {
    // string has one value
    $custom_emails = $email_address;
}

// Next, check for staff emails
$emails = '';
$count = 0;
foreach ($email_staff as $staff) {
    $emails .= antispambot(get_field('staff_email', $staff->ID)) .',';
    $count++;
}
if ($count > 1) {
    // multiple staff emails
    $staff_emails = explode(',', $emails);
    $staff_emails = array_filter($staff_emails);
} else {
    // single staff email
    $staff_emails = str_replace(',', '', $emails);
}

// Prep output
if (sizeof($custom_emails) > 1) $custom_emails = implode(', ', $custom_emails);
if (sizeof($staff_emails) > 1) $staff_emails = implode(', ', $staff_emails);
$all_emails = array( $custom_emails, $staff_emails );

if ( empty($all_emails) ) {
    // Default if no emails were added
    $all_emails = antispambot($email_default);
} else {
    $all_emails = array_filter($all_emails);
    $all_emails = implode(', ', $all_emails);
}

// echo '<pre>'; var_dump($all_emails); echo '</pre>';

// Save the email string as a session variable
// so it doesn't have to be printed on the page
session_start();
$_SESSION['form-recipient'] = $all_emails;
?>

<input type="hidden" name="return" value="<?php echo rtrim(get_the_permalink(), '/'); ?>">