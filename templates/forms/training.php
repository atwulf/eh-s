<?php
/**
 * Template partial to display different form types on Trainings.
 */

$form_type = get_field('training_form_type');

if ( $form_type == 'quiz' ) { # Show Quiz Questions loop

    // Get Quizzes taxonomy first
    $quiz = get_field('related_quiz');

    if ( isset($_GET['complete']) ) { # Show Success Screen ?>

        <h2 id="page-title"><?php the_title(); ?> Completion Quiz</h2>

        <h3>Quiz submitted!</h3>
        <p>Thank you! Your answers have been submitted. A proctor will contact you with your results as soon as they are graded.</p>
        <p>A copy of your quiz answers will be emailed to you. Please print a copy for your records.</p>

    <?php }
    if ( isset($_GET['fail']) ) { # Show Failure Screen ?>

        <h2 id="page-title"><?php the_title(); ?> Completion Quiz</h2>

        <h3>An error occurred.</h3>
        <p>We're sorry! Something went wrong. Please <a href="javascript:history.go(-1)">go back to the completion quiz page</a> and make sure everything was entered correctly.</p>
        <p>We apologize for any inconvenience.</p>

    <?php }
    if ( ! isset($_GET['complete']) && ! isset($_GET['fail']) ) { # Loop quiz questions ?>

        <h2><?php the_title(); ?> Completion Quiz</h2>

        <p>Please fill out the following information and answer all questions.</p>
                    
        <form action="<?php bloginfo('template_url'); ?>/forms.php" method="post" class="quiz">

            <div class="grid">
                <div class="col-1-2">
                    <label for="first-name">
                        <span>Your First Name</span>
                        <input type="text" id="first-name" name="first-name">
                    </label>
                </div>
                <div class="col-1-2">
                    <label for="last-name">
                        <span>Your Last Name</span>
                        <input type="text" id="last-name" name="last-name">
                    </label>
                </div>
            </div>

            <div class="grid">
                <div class="col-1-2">
                    <label for="email-address">
                        <span>Your Email</span>
                        <input type="text" id="email-address" name="email-address">
                    </label>
                </div>
                <div class="col-1-2">
                    <label for="supervisor">
                        <span>Your Supervisor/PI</span>
                        <input type="text" id="supervisor" name="supervisor">
                    </label>
                </div>
            </div>

            <div class="grid">
                <div class="col-1-2">
                    <label for="proctor-name">
                        <span>Lab Proctor's Name (optional)</span>
                        <input type="text" id="proctor-name" name="proctor-name">
                    </label>
                </div>
                <div class="col-1-2">
                    <label for="proctor-email">
                        <span>Lab Proctor's Email (optional)</span>
                        <input type="text" id="proctor-email" name="proctor-email">
                    </label>
                </div>
            </div>

            <div class="grid">
                <div class="col-1-2">
                    <label for="role">
                        <span>Your Role</span>
                        <select name="role" id="role">
                            <option value="Principal Investigator">Principal Investigator</option>
                            <option value="Lab Proctor">Lab Proctor</option>
                            <option value="Generator">Generator</option>
                            <option value="Faculty/Staff">Faculty/Staff</option>
                            <option value="Undergrad">Student - Undergrad</option>
                            <option value="Graduate Student">Student - Graduate</option>
                            <option value="PostDoc">Postdoctoral</option>
                        </select>
                    </label>
                </div>
                <div class="col-1-2 grid">
                    <label for="building-code" class="col-1-2">
                        <span>Building Code</span>
                        <?php get_template_part('templates/forms/building'); ?>
                    </label>
                    <label for="room-number" class="col-1-2">
                        <span>Room Number</span>
                        <input type="text" id="room-number" name="room-number">
                    </label>
                </div>
            </div>

            <?php
            $questions = get_field('related_quiz');

            $args = array(
                'post_type' => 'training_questions',
                'posts_per_page' => -1,
                'order' => 'ASC',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'for_training',
                        'field' => 'id',
                        'terms' => $questions->term_id
                    )
                )
            );

            $questions_loop = new WP_Query( $args );

            if ( $questions_loop->have_posts() ) {
                echo '<ol class="question-list">';

                while ( $questions_loop->have_posts() ) {

                    $questions_loop->the_post();
                    get_template_part('entry', 'quiz');

                }

                echo '</ol>';
            } wp_reset_postdata();
            ?>

            <?php get_template_part('templates/forms/recipient'); ?>
            <input type="hidden" name="q" value="quiz">
            <input type="hidden" name="quiz-title" value="<?php the_title(); ?>">

            <button class="button-gold">Submit Answers</button>

        </form>

    <?php } // End QUIZ FORM template

} elseif ( $form_type == 'info' ) { # Show Learn More form

    if ( isset($_GET['complete']) ) { # Success! ?>

        <h2 id="page-title"><?php echo $signup; the_title(); ?></h2>

        <h3>Success!</h3>

        <p>Thank you! Your information has been submitted. A proctor will contact you with more information as soon as possible.</p>

        <p>A copy of your information will be emailed to you. Please print a copy for your records.</p>

    <?php }
    if ( isset($_GET['fail']) ) { # Failure! ?>

        <h2 id="page-title"><?php echo $signup; the_title(); ?></h2>

        <h3>An error occurred.</h3>

        <p>We're sorry! Something went wrong. Please <a href="javascript:history.go(-1)">go back to the sign up page</a> and make sure everything was entered correctly.</p>

        <p>We apologize for any inconvenience.</p>

    <?php }
    if ( ! isset($_GET['complete']) && ! isset($_GET['fail']) ) { # Spit out the LEARN MORE form ?>

        <h2>Learn More About <?php the_title(); ?></h2>

        <p>Interested in taking this training, or not sure if you need to take it? Send us your information and we'll get back to you about scheduling a session.</p>
                    
        <form action="<?php bloginfo('template_url'); ?>/forms.php" method="post" class="quiz">

            <div class="grid">
                <div class="col-1-2">
                    <label for="first-name">
                        <span>Your First Name</span>
                        <input type="text" id="first-name" name="first-name">
                    </label>
                </div>
                <div class="col-1-2">
                    <label for="last-name">
                        <span>Your Last Name</span>
                        <input type="text" id="last-name" name="last-name">
                    </label>
                </div>
            </div>

            <div class="grid">
                <div class="col-1-2">
                    <label for="email-address">
                        <span>Your Email</span>
                        <input type="text" id="email-address" name="email-address">
                    </label>
                </div>
                <div class="col-1-2">
                    <label for="supervisor">
                        <span>Your Supervisor/PI</span>
                        <input type="text" id="supervisor" name="supervisor">
                    </label>
                </div>
            </div>

            <div class="grid">
                <div class="col-1-2">
                    <label for="role">
                        <span>Your Role</span>
                        <select name="role" id="role">
                            <option value="Principal Investigator">Principal Investigator</option>
                            <option value="Lab Proctor">Lab Proctor</option>
                            <option value="Generator">Generator</option>
                            <option value="Faculty/Staff">Faculty/Staff</option>
                            <option value="Undergrad">Student - Undergrad</option>
                            <option value="Graduate Student">Student - Graduate</option>
                            <option value="PostDoc">Postdoctoral</option>
                        </select>
                    </label>
                </div>
            </div>

            <?php get_template_part('templates/forms/recipient'); ?>
            <input type="hidden" name="q" value="training_info">
            <input type="hidden" name="quiz-title" value="<?php the_title(); ?>">

            <button class="button-gold">
                Submit
            </button>

        </form>

    <?php } // END More Info form template

} elseif ( $form_type == 'schedule' ) { # Show Sign Up form with upcoming sessions

    if ( isset($_GET['complete']) ) { # Success! ?>

        <h2 id="page-title"><?php echo $signup; the_title(); ?></h2>

        <h3>Success!</h3>

        <p>Thank you! Your information has been submitted. A proctor will contact you with more information as soon as possible.</p>

        <p>A copy of your information will be emailed to you. Please print a copy for your records.</p>

    <?php }
    if ( isset($_GET['fail']) ) { # Failure! ?>

        <h2 id="page-title"><?php echo $signup; the_title(); ?></h2>

        <h3>An error occurred.</h3>

        <p>We're sorry! Something went wrong. Please <a href="javascript:history.go(-1)">go back to the sign up page</a> and make sure everything was entered correctly.</p>

        <p>We apologize for any inconvenience.</p>

    <?php }
    if ( ! isset($_GET['complete']) && ! isset($_GET['fail']) ) { # Spit out the SIGN UP form ?>

        <h2>Sign Up for <?php the_title(); ?></h2>
        <p>Need to take this training, or want to find out if you need to? Fill out this form and we'll get back to you right away.</p>
                    
        <form action="<?php bloginfo('template_url'); ?>/forms.php" method="post" class="quiz">

            <div class="grid">
                <div class="col-1-2">
                    <label for="first-name">
                        <span>Your First Name</span>
                        <input type="text" id="first-name" name="first-name">
                    </label>
                </div>
                <div class="col-1-2">
                    <label for="last-name">
                        <span>Your Last Name</span>
                        <input type="text" id="last-name" name="last-name">
                    </label>
                </div>
            </div>

            <div class="grid">
                <div class="col-1-2">
                    <label for="email-address">
                        <span>Your Email</span>
                        <input type="text" id="email-address" name="email-address">
                    </label>
                </div>
                <div class="col-1-2">
                    <label for="supervisor">
                        <span>Supervisor/PI</span>
                        <input type="text" id="supervisor" name="supervisor">
                    </label>
                </div>
            </div>

            <div class="grid">
                <div class="col-1-2">
                    <label for="role">
                        <span>Role</span>
                        <select name="role" id="role">
                            <option value="Principal Investigator">Principal Investigator</option>
                            <option value="Lab Proctor">Lab Proctor</option>
                            <option value="Generator">Generator</option>
                            <option value="Faculty/Staff">Faculty/Staff</option>
                            <option value="Undergrad">Student - Undergrad</option>
                            <option value="Graduate Student">Student - Graduate</option>
                            <option value="PostDoc">Postdoctoral</option>
                        </select>
                    </label>
                </div>
                <div class="col-1-2">
                <?php $sessions = get_field('upcoming_sessions');
                if ($sessions) :
                    $upcoming_sessions = explode("\n", trim( $sessions )); ?>
                    <label for="course-date">
                        <span>Course Date</span>
                        <select name="course-date" id="course-date">
                            <?php foreach ($upcoming_sessions as $ucs) {
                                echo '<option value="'. $ucs .'">'. $ucs .'</option>';
                            } ?>
                        </select>
                    </label>
                <?php endif; ?>
                </div>
            </div>

            <?php get_template_part('templates/forms/recipient'); ?>
            <input type="hidden" name="q" value="training_signup">
            <input type="hidden" name="quiz-title" value="<?php the_title(); ?>">

            <button class="button-gold">
                Sign Up
            </button>

        </form>

    <?php } // END More Info form template

} elseif( $form_type == 'offsite' ) { # Shouldn't display

    // umm, nothing?

}

?>

