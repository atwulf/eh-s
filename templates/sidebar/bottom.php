<?php
// Check for Don't Show checkbox
if ( get_field('dont_show') !== 'show' && get_field('dont_show') !== false ) { ?>

    <?php // Then, yeah, don't show anything. ?>

<?php } // By Default, fill the More Info panel from this page
elseif ( get_field('more_information') ) { ?>

    <div class="module more-info">
        
        <h4>More Information</h4>
        <?php the_field('more_information'); ?>

    </div>

<?php } // Training Quizzes get the "Answered Questions meter" panel
elseif ( get_field('training_form_type') == 'quiz' && $_GET['page'] == 'form' && ! isset($_GET['complete']) ) { ?>
    <div class="module more-info">
        
        <h4>Answered Questions</h4>
        <div class="progress">
            <div class="tray"><div class="bar"></div></div>
            <div class="count"></div>
        </div>

    </div>
<?php } // If More Info is not defined, inherit from parent(s)
else {

    if ( get_post_type() == 'page' && get_the_title() !== 'Resources' ) {
        $parents = ehs_get_page_parents( get_the_ID() ); ?>
        
        <div class="module more-info">

            <h4>More Information</h4>
            <?php // Get Parent More Info Panels
            $top_parent = get_field('more_information', $parents['top']);
            $immed_parent = get_field('more_information', $post->post_parent);

            if ( $immed_parent ) { # Pull from immediate parent first
                echo $immed_parent;

            } else { # Otherwise, pull from the topmost parent
                echo $top_parent;

            } ?>
            
        </div>

    <?php }

} ?>