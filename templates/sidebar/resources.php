<?php if ( isset( $_GET ) ) {
    $filters = array(
        'keyword' => $_GET['kw'],
        'date' => $_GET['date'],
        'concern' => $_GET['con'],
        'type' => $_GET['type'],
        'unit' => $_GET['unit'],
        'sort' => $_GET['sort'],
        'order' => $_GET['order']
    );
} ?>

<div class="module find-a-resource">
    <form action="<?php the_permalink(); ?>" method="get">

    <h4>Find a Resource</h4>
    <label for="kw">
        <span>By Keyword</span>
        <input type="text" id="kw" name="kw" placeholder="Enter some text..." <?php if ( isset( $filters['keyword'] ) ) echo 'value="'. $filters['keyword'] .'"'; ?>>
    </label>
    <label for="date">
        <span>By Last Update</span>
        <select name="date" id="date">
            <option <?php if ( $filters['updated'] == 'all' ) echo 'selected'; ?> value="all">All dates</option>
            <option <?php if ( $filters['updated'] == 'last-30-days' ) echo 'selected'; ?> value="last-30-days">Last 30 days</option>
            <option <?php if ( $filters['updated'] == 'last-6-months' ) echo 'selected'; ?> value="last-6-months">Last 6 months</option>
            <option <?php if ( $filters['updated'] == 'last-year' ) echo 'selected'; ?> value="last-year">Last year</option>
        </select>
    </label>
    <label for="con">
        <span>By Document Concern</span>
        <select name="con" id="con">
            <option value="all" <?php if ( $filters['concern'] == 'all' ) echo 'selected'; ?>>All concerns</option>
            <?php // Loop all document concerns
            $doc_concerns = get_terms('doc_concerns');
            foreach ( $doc_concerns as $concern ) {
                echo '<option value="'. $concern->slug .'"';
                if ( $filters['concern'] == $concern->slug ) echo ' selected';
                echo '>'. $concern->name .'</option>';
            }
            ?>
        </select>
    </label>
    <label for="type">
        <span>By Document Type</span>
        <select name="type" id="type">
            <option value="all" <?php if ( $filters['type'] == 'all' ) echo 'selected'; ?>>All types</option>
            <?php // Loop all document types
            $doc_types = get_terms('doc_type');
            foreach ( $doc_types as $type ) {
                echo '<option value="'. $type->slug .'"';
                if ( $filters['type'] == $type->slug ) echo ' selected';
                echo '>'. $type->name .'</option>';
            }
            ?>
        </select>
    </label>
    <label for="unit">
        <span>By Unit</span>
        <select name="unit" id="unit">
            <option value="all" <?php if ( $filters['unit'] == 'all' ) echo 'selected'; ?>>All units</option>
            <?php // Loop all departments
            $departments = get_terms('departments');
            foreach ( $departments as $type ) {
                echo '<option value="'. $type->slug .'"';
                if ( $filters['unit'] == $type->slug ) echo ' selected';
                echo '>'. $type->name .'</option>';
            }
            ?>
        </select>
    </label>
    <?php /* CHECKBOX FILTER
    <label for="gov">
        <input <?php if ( $filters['government'] == 'on' ) echo 'checked'; ?> type="checkbox" name="gov" id="gov">
        <span>Government related</span>
    </label> */ ?>

    <?php if ( isset($_GET['sort']) ) : ?>
        <input type="hidden" name="sort" value="<?php echo $_GET['sort']; ?>">
    <?php endif; ?>

    <button class="button-gold">Filter Resources</button>

    </form>
</div>