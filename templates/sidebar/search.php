<div class="module subpages">
        
    <h4>Refine Your Search</h4>
    <?php if ( isset( $_GET ) ) {
    $filters = array(
        'search' => $_GET['s']
    );
    } ?>

    <form action="<?php echo home_url('/'); ?>" method="get">

        <label for="s">
            <span>By Keyword</span>
            <input type="text" id="s" name="s" placeholder="Enter some text..." <?php if ( isset( $filters['search'] ) ) echo 'value="'. $filters['search'] .'"'; ?>>
        </label>

        <label for="post_type">
            <span>By Type</span>
            <select name="post_type">
                <option value="all" <?php if ( $_GET['post_type'] == 'all' ) echo 'selected'; ?>>All types</option>
                <option value="trainings" <?php if ( $_GET['post_type'] == 'trainings' ) echo 'selected'; ?>>Trainings</option>
                <option value="audits" <?php if ( $_GET['post_type'] == 'audits' ) echo 'selected'; ?>>Lab Audits</option>
                <option value="resources" <?php if ( $_GET['post_type'] == 'resources' ) echo 'selected'; ?>>Resources</option>
            </select>
        </label>

        <label for="post_parent">
            <span>By Concern</span>
            <select name="post_parent">
                <option value="0">Any</option>
                <option value="<?php $page = get_page_by_title('Lab Support'); echo $page->ID; ?>"<?php
                    if ( $page->ID == $_GET['post_parent']) echo ' selected'
                ?>>Research/Laboratory</option>
                <option value="<?php $page = get_page_by_title('Campus Support'); echo $page->ID; ?>"<?php
                    if ( $page->ID == $_GET['post_parent']) echo ' selected'
                ?>>Operations/Campus</option>
            </select>
        </label>

        <button class="button-gold">Search</button>
    
    </form>

</div>