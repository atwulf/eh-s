<?php // TRAINING - entry-training.php
if ( get_post_type() == 'trainings' ) { ?>

    <div class="module subpages">
        
        <h4><a href="<?php the_permalink(); ?>" <?php if ( ! $_GET ) echo 'class="current"'; ?>><?php the_title(); ?> Overview</a></h4>

        <?php if ( get_field('course_material') ) : ?>
        <h4><a href="?page=course_material" <?php if ( $_GET['page'] == 'course_material' ) echo 'class="current"'; ?>>Course Material</a></h4>
        <?php endif; ?>

        <?php // Get form page link
        $form_type = get_field('training_form_type');
        $text = 'Null';
        if ($form_type == 'quiz') $text = 'Completion Quiz';
        if ($form_type == 'info') $text = 'Request Information';
        if ($form_type == 'schedule') $text = 'Sign Up for Training';
        if ($form_type == 'offsite') $text = 'Sign Up for Training';
        if ($form_type == 'offsite' && get_field('course_material')) $text = 'Completion Quiz';

        $href = '?page=form';
        if ($form_type == 'offsite') $href = get_field('offsite_url');

        if ( $form_type !== 'hide' ) {
            echo '<h4><a href="'. $href .'"';
            if ($_GET['page'] == 'form') echo ' class="current"';
            if ($form_type == 'offsite') echo ' target="_blank"';
            echo '>'. $text .'</a></h4>';
        } ?>

    </div>

<?php } // RESOURCES - entry-resource.php
elseif ( get_post_type() == 'resources' ) { ?>

    <?php if ( get_field('resource_type') == 'page' ) : ?>

    <div class="module toc">
        
        <span class="hide-toc">(hide)</span>
        <h4>Table of Contents</h4>
        <ol class="toc-list"></ol>

    </div>

    <?php else : ?>

    <div class="module">

        <h4>Additional Information</h4>
        <p>Last updated:<br/>
        <em><?php echo get_the_date('F j, Y'); ?></em></p>
        <p>Intended for:<br/>
        <em><?php the_field('intended_for'); ?></em></p>
        <p>Unit:<br/>
        <em>
        <?php $departments = wp_get_post_terms( get_the_ID(), 'departments' );
        end($departments);
        $last_key = key($departments);
        foreach ( $departments as $key => $dept ) {
            echo $dept->name;
            if ( $key !== $last_key ) echo ', ';
        }
        ?></em></p>
        
    </div>

    <?php endif; ?>

<?php } // DEFAULT PAGE - page.php
elseif ( get_post_type() == 'page' ) { ?>

    <?php
    $parents = ehs_get_page_parents( get_the_ID() );
    $top_page = $parents['top'];
    $parents = $parents['parents']; ?>

    <div class="module subpages">

        <h4><a href="<?php echo get_the_permalink( $top_page ); ?>" <?php if ( sizeof($parents) === 0 ) echo 'class="current"'; ?>><?php echo get_the_title($top_page); ?></a></h4>
        
        <?php $children = get_pages('sort_column=menu_order&child_of='. $top_page);
            foreach ( $children as $child ) {
                if ( $child->post_parent == $top_page ||
                     $child->post_parent == get_the_ID() ||
                     sizeof( $parents ) > 1 && in_array( $child->post_parent, $parents[1] ) ) { ?>

                <?php if ( sizeof( get_post_ancestors($child->ID) ) > 1 ) { echo '<p>'; } else { echo '<h4>'; } ?>
                <a href="<?php echo get_the_permalink( $child->ID ); ?>"
                    <?php if ( get_the_permalink() == get_the_permalink( $child->ID ) ) echo 'class="current"'; ?>><?php echo $child->post_title; ?></a>
                <?php if ( sizeof( get_post_ancestors($child->ID) ) > 1 ) { echo '</p>'; } else { echo '</h4>'; } ?>

                <?php } ?>
            <?php } ?>

        <?php if ( get_the_title() == 'Staff Directory' ) {
            $units = get_terms('departments');
            foreach ( $units as $unit ) {
                echo '<p><a href="#'. $unit->slug .'">'. $unit->name .'</a></p>';
            }
        } ?>

    </div>

<?php } // DEFAULT POST - entry-post.php
elseif ( get_post_type() == 'post' || is_archive() ) { ?>

    <div class="module subpages">

        <?php $units = get_terms('departments');
        foreach ( $units as $unit ) {
            echo '<h4><a href="'. get_term_link( $unit->slug, 'departments' ) .'">'. $unit->name .'</a></h4>';
        } ?>

    </div>

<?php } ?>