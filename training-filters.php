<?php // Maintain existing query string
$query = '?';

if ( isset ($_GET) ) {
    foreach ( $_GET as $key => $val ) {
        $val = str_replace(' ','+', $val);
        if ( $key == 'v' ) $view = $val;
        if ( $key !== 'v' ) $query .= $key .'='. $val .'&';
    }
} ?>
<div class="grid">
    <div class="col-1-2">

        <label><span>View list as</span></label>
        <a href="<?php echo $query; ?>v=list" class="button-secondary <?php if ( $_GET['v'] == 'list' || !isset($_GET['v']) ) echo 'active'; ?>">Titles</a>
        <a href="<?php echo $query; ?>v=detail" class="button-secondary <?php if ( $_GET['v'] == 'detail' ) echo 'active'; ?>">Details</a>

    </div>
    <div class="col-1-2">

        <?php // Get Intended For terms
        $intended_for = get_terms( 'intended_for' );

        // Get Research With terms
        $research_with = get_terms('research_with'); ?>

        <label for="intended_for"><span>Show trainings intended for</span>
        <select name="intended_for" id="intended_for" class="filters">
            <option value="null">Anyone</option>
        <?php foreach ( $intended_for as $for ) { ?>
            
            <option value="<?php echo $for->slug; ?>" <?php if ( $_GET['f'] == $for->slug ) echo 'selected'; ?>><?php echo ucfirst($for->name); ?></option>

        <?php } ?>
        </select></label><br/>
        
        <label for="research_with"><span>Show trainings for research with</span>
        <select name="research_with" id="research_with" class="filters" <?php if ( $_GET['f'] !== 'researchers' && $_GET['f'] !== 'null' && isset($_GET['f']) ) echo 'disabled'; ?>>
            <option value="null"></option>
        <?php foreach ( $research_with as $with ) { ?>
            
            <option value="<?php echo $with->slug; ?>" <?php if ( $_GET['w'] == $with->slug ) echo 'selected'; ?>><?php echo ucfirst($with->name); ?></option>

        <?php } ?>
        </select></label>

    </div>

</div>